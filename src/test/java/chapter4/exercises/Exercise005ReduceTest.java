package chapter4.exercises;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Exercise005ReduceTest {
    @Test
    public void test001ReduceSum1Example() {
        List<Integer> input = Arrays.asList(1, 2, 3, 4, 5);
        int result = 15;

        assertEquals(result, Exercise005Reduce.sum1(input));
    }

    @Test
    public void test002ReduceSum2() {
        List<Integer> input = Arrays.asList(1, 2, 3, 5, 5);
        int result = 16;

        assertEquals(result, Exercise005Reduce.sum2(input));
    }

    @Test
    public void test003ReduceSum3() {
        List<Integer> input = Arrays.asList(1, 2, 4, 5, 5);
        int result = 17;

        assertEquals(result, Exercise005Reduce.sum3(input));
    }

    @Test
    public void test004ReduceSum4() {
        List<Integer> input = Arrays.asList(1, 3, 4, 5, 5);
        int result = 18;

        assertEquals(result, Exercise005Reduce.sum4(input));
    }

    @Test
    public void test005ReduceMaxFromCollection() {
        List<Integer> input = Arrays.asList(1, 3, 4, 5, 5);
        Optional<Integer> result = Optional.of(5);

        assertEquals(result, Exercise005Reduce.maxFromCollection(input));
    }

    @Test
    public void test005ReduceMaxFromCollectionEmpty() {
        List<Integer> input = new ArrayList<>();
        Optional<Integer> result = Optional.empty();

        assertEquals(result, Exercise005Reduce.maxFromCollection(input));
    }

    @Test
    public void test006ReduceMyReduceSum() {
        List<Integer> input = Arrays.asList(2, 3, 4, 5, 5);
        int result = 19;

        assertEquals(result, Exercise005Reduce.myReduce(input, 0, Integer::sum));
    }

    @Test
    public void test006ReduceMyReduceMultiplication() {
        List<Integer> input = Arrays.asList(2, 3, 4, 5, 5);
        int result = 600;

        assertEquals(result, Exercise005Reduce.myReduce(input, 1, Math::multiplyExact));
    }

    @Test
    public void test007ReduceMyReduceGenericStringConcat() {
        List<String> input = Arrays.asList("A, ", "B, ", "C, ", "Easy as ", "1, 2, ", "3");
        String result = "A, B, C, Easy as 1, 2, 3";

        assertEquals(result, Exercise005Reduce.myReduce(input, "", (a, b) -> a + b));
    }
}
