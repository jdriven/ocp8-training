package chapter4.exercises;

public class WorkshopUtils {
    public static boolean isLambda(Object obj) {
        return obj.getClass().toString().contains("$$Lambda$");
    }
}
