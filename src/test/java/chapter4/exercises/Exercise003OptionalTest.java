package chapter4.exercises;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

import java.io.PrintStream;
import java.util.Optional;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Exercise003OptionalTest {
    @Test
    public void test001Optional() {
        assertEquals(Optional.of("Test"), Exercise003Optional.returnOptional("Test"));
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    @Test(expected = NullPointerException.class)
    public void test002OptionalException() {
        Exercise003Optional.returnOptional(null);
    }

    @Test
    public void test003NullSafeOptional() {
        assertEquals(Optional.of("Test"), Exercise003Optional.nullSafeOptional("Test"));
        assertEquals(Optional.empty(), Exercise003Optional.nullSafeOptional(null));
    }

    @Test
    public void test004IsHasValue() {
        assertTrue(Exercise003Optional.hasValue(Optional.of("")));
        assertTrue(Exercise003Optional.hasValue(Optional.of(0)));
        assertFalse(Exercise003Optional.hasValue(Optional.empty()));
    }

    @Test
    public void test005Default() {
        final String value = "VALUE";
        assertEquals(value, Exercise003Optional.getValueOrDefault(Optional.of(value)));
        assertEquals(Exercise003Optional.SENSIBLE_DEFAULT, Exercise003Optional.getValueOrDefault(Optional.empty()));
    }

    @Test
    public void test006NullSafeOptional_EmptyOptionalIfEmptyString() {
        assertEquals(Optional.of("Test"), Exercise003Optional.nullSafeOptional_EmptyOptionalIfEmptyString("Test"));
        assertEquals(Optional.empty(), Exercise003Optional.nullSafeOptional_EmptyOptionalIfEmptyString(null));
        assertEquals(Optional.empty(), Exercise003Optional.nullSafeOptional_EmptyOptionalIfEmptyString(""));
    }

    @Test
    public void test007MultiplyByTwo() {
        assertEquals(Optional.empty(), Exercise003Optional.multiplyByTwo(Optional.empty()));
        test007MultiplyByTwo(-1);
        test007MultiplyByTwo(0);
        test007MultiplyByTwo(1);
        test007MultiplyByTwo(23);
    }

    private void test007MultiplyByTwo(final int num) {
        assertEquals(Optional.of(2 * num), Exercise003Optional.multiplyByTwo(Optional.of(num)));
    }

    @Test
    public void test008IfPresent() {
        test008IfPresent(Optional.of("FooBar"));
        test008IfPresent(Optional.of(""));
        test008IfPresent(Optional.empty());
    }

    private void test008IfPresent(final Optional<String> input) {
        final PrintStream out = mock(PrintStream.class);
        System.setOut(out);

        Exercise003Optional.printValue(input);

        if (!input.isPresent()) {
            verifyZeroInteractions(out);
        } else {
            verify(out).println(input.get());
        }
    }
}
