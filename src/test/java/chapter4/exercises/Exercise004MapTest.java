package chapter4.exercises;

import chapter4.exercises.util.Person;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Exercise004MapTest {
    @Test
    public void test001MapNegate() {
        List<Integer> input = Arrays.asList(1, -3, 4, 10, -33);
        List<Integer> result = Arrays.asList(-1, 3, -4, -10, 33);


        assertEquals(result, new Exercise004Map().negate(input));
    }

    @Test
    public void test002MapNames() {
        Person p = new Person("Henk", 3);
        Person p2 = new Person("Jan", 4);
        List<String> result = Arrays.asList("Henk", "Jan");

        assertEquals(result, new Exercise004Map().names(Arrays.asList(p, p2)));
    }

    @Test
    public void test003MapLowerCaseNames() {
        Person p = new Person("Henk", 3);
        Person p2 = new Person("Jan", 4);
        List<String> result = Arrays.asList("henk", "jan");

        assertEquals(result, new Exercise004Map().lowerCaseNames(Arrays.asList(p, p2)));
    }

    @Test
    public void test003MapLengthLongestName() {
        List<String> input = Arrays.asList("Henk", "Jan", "Pieter");
        int result = 6;

        assertEquals(result, new Exercise004Map().lengthLongestName(input));
    }

    @Test
    public void test003MapLengthLongestNameEmptyList() {
        List<String> input = new ArrayList<>();
        int result = 0;

        assertEquals(result, new Exercise004Map().lengthLongestName(input));
    }

    @Test
    public void test003MapLengthLongestName2() {
        List<String> input = Arrays.asList("Henk", "Jan", "Pieter");
        int result = 6;

        assertEquals(result, new Exercise004Map().lengthLongestName2(input));
    }

    @Test
    public void test003MapLengthLongestNameEmptyList2() {
        List<String> input = new ArrayList<>();
        int result = 0;

        assertEquals(result, new Exercise004Map().lengthLongestName2(input));
    }

    @Test
    public void test003MapShortestName() {
        List<String> input = Arrays.asList("Henk", "Jan", "Pieter");
        Optional<String> result = Optional.of("Jan");

        assertEquals(result, Exercise004Map.shortestName(input));
    }

    @Test
    public void test003MapShortestNameEmpty() {
        List<String> input = new ArrayList<>();
        Optional<String> result = Optional.empty();

        assertEquals(result, Exercise004Map.shortestName(input));
    }
}
