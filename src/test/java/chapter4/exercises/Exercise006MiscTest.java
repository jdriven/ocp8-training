package chapter4.exercises;

import chapter4.exercises.util.Person;
import chapter4.exercises.util.Record;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Exercise006MiscTest {
    @Test
    public void test001MiscAllAdults() {
        List<Person> input = Arrays.asList(
            new Person("a", 19),
            new Person("b", 20),
            new Person("c", 40)
        );

        assertTrue(new Exercise006Misc().allAdults(input));
    }

    @Test
    public void test001MiscNotAllAdults() {
        List<Person> input = Arrays.asList(
            new Person("a", 17),
            new Person("b", 20),
            new Person("c", 40)
        );

        assertFalse(new Exercise006Misc().allAdults(input));
    }

    @Test
    public void test001MiscAllAdultsWithEmptyList() {
        List<Person> input = new ArrayList<>();

        assertTrue(new Exercise006Misc().allAdults(input));
    }

    @Test
    public void test002MiscNameStartsWithC() {
        List<Person> input = Arrays.asList(
            new Person("anthon", 19),
            new Person("bobby", 20),
            new Person("chris", 40),
            new Person("Chris", 42)
        );
        List<Person> result = Arrays.asList(
            new Person("chris", 40)
        );

        assertEquals(result, new Exercise006Misc().nameStartsWith(input, 'c'));
    }

    @Test
    public void test003MiscByTopic() {
        List<Record> input = Arrays.asList(
            new Record("topic1", "content1"),
            new Record("topic1", "content2"),
            new Record("topic2", "content3")
        );
        List<Record> topic1 = Arrays.asList(
            new Record("topic1", "content1"),
            new Record("topic1", "content2")
        );
        List<Record> topic2 = Collections.singletonList(
            new Record("topic2", "content3")
        );

        Map<String, List<Record>> result = new HashMap<>();
        result.put("topic1", topic1);
        result.put("topic2", topic2);

        assertEquals(result, new Exercise006Misc().byTopic(input));
    }

    @Test
    public void test004MiscShortestPerTopic() {
        List<Record> input = Arrays.asList(
            new Record("topic1", "1"),
            new Record("topic1", "12"),
            new Record("topic2", "123"),
            new Record("topic2", "12")
        );

        Map<String, Record> result = new HashMap<>();
        result.put("topic1", new Record("topic1", "1"));
        result.put("topic2", new Record("topic2", "12"));

        assertEquals(result, new Exercise006Misc().shortestPerTopic(input));
    }

    @Test
    public void test005MiscStatistics1() {
        List<Record> input = Arrays.asList(
            new Record("topic1", "1"),
            new Record("topic1", "12"),
            new Record("topic2", "123"),
            new Record("topic2", "12")
        );

        Map<String, Double> result = new HashMap<>();
        result.put("shortest", 1.0);
        result.put("longest", 3.0);
        result.put("average", 2.0);

        assertEquals(result, new Exercise006Misc().statistics1(input));
    }

    @Test
    public void test006MiscStatistics2() {
        List<Record> input = Arrays.asList(
            new Record("topic1", "1"),
            new Record("topic1", "12"),
            new Record("topic2", "123"),
            new Record("topic2", "12")
        );

        Map<String, Double> result = new HashMap<>();
        result.put("shortest", 1.0);
        result.put("longest", 3.0);
        result.put("average", 2.0);

        assertEquals(result, new Exercise006Misc().statistics2(input));
    }

    @Test
    public void test007MiscStatistics3() {
        List<Record> input = Arrays.asList(
            new Record("topic1", "1"),
            new Record("topic1", "12"),
            new Record("topic2", "123"),
            new Record("topic2", "12")
        );

        Map<String, Double> result = new HashMap<>();
        result.put("shortest", 1.0);
        result.put("longest", 3.0);
        result.put("average", 2.0);

        assertEquals(result, new Exercise006Misc().statistics3(input));
    }
}
