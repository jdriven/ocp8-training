package utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.stream.Collectors;

public class LoggingInputStream extends InputStream {

    private final String prefix;
    private final InputStream wrappedStream;

    private LoggingInputStream(String prefix, InputStream wrappedStream) {
        this.prefix = prefix;
        this.wrappedStream = wrappedStream;
    }

    public static InputStream wrap(InputStream toWrap) {
        return new LoggingInputStream(toWrap.getClass().getSimpleName(), toWrap);
    }

    @Override
    public int read() throws IOException {
        log("read");
        return wrappedStream.read();
    }

    @Override
    public int read(byte[] b) throws IOException {
        log("read", "[" + b.length + " bytes]");
        return wrappedStream.read(b);
    }

    @Override
    public int read(byte[] b, int off, int len) throws IOException {
        log("read", "[" + b.length + " bytes]", off, len);
        return wrappedStream.read(b, off, len);
    }

    @Override
    public long skip(long n) throws IOException {
        log("skip", n);
        return wrappedStream.skip(n);
    }

    @Override
    public int available() throws IOException {
        log("available");
        return wrappedStream.available();
    }

    @Override
    public void close() throws IOException {
        log("close");
        wrappedStream.close();
    }

    @Override
    public synchronized void mark(int readlimit) {
        log("mark", readlimit);
        wrappedStream.mark(readlimit);
    }

    @Override
    public synchronized void reset() throws IOException {
        log("reset");
        wrappedStream.reset();
    }

    @Override
    public boolean markSupported() {
        log("markSupported");
        return wrappedStream.markSupported();
    }

    private void log(String methodName, Object... args) {
        System.out.println(String.format("%s.%s(%s)", prefix, methodName, Arrays.stream(args).map(String::valueOf).collect(Collectors.joining(", "))));
    }
}
