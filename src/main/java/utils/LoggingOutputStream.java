package utils;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.stream.Collectors;

public class LoggingOutputStream extends OutputStream {

    private final String prefix;
    private final OutputStream wrappedStream;

    private LoggingOutputStream(String prefix, OutputStream wrappedStream) {
        this.prefix = prefix;
        this.wrappedStream = wrappedStream;
    }

    public static OutputStream wrap(OutputStream toWrap) {
        return new LoggingOutputStream(toWrap.getClass().getSimpleName(), toWrap);
    }

    @Override
    public void write(int b) throws IOException {
        log("write", b);
        wrappedStream.write(b);
    }

    @Override
    public void write(byte[] b) throws IOException {
        log("write", "[" + b.length + " bytes]");
        wrappedStream.write(b);
    }

    @Override
    public void write(byte[] b, int off, int len) throws IOException {
        log("write", "[" + b.length + " bytes]", off, len);
        wrappedStream.write(b, off, len);
    }

    @Override
    public void flush() throws IOException {
        log("flush");
        wrappedStream.flush();
    }

    @Override
    public void close() throws IOException {
        log("close");
        wrappedStream.close();
    }

    private void log(String methodName, Object... args) {
        System.out.println(String.format("%s.%s(%s)", prefix, methodName, Arrays.stream(args).map(String::valueOf).collect(Collectors.joining(", "))));
    }
}
