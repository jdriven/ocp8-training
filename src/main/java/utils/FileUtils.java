package utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.util.stream.Collectors;

/**
 * Not part of the exam, but helper methods for file operations in the examples.
 */
public class FileUtils {

    public static File createTempFile() throws IOException {
        return createTempFile(null);
    }

    public static File createTempFile(String content) throws IOException {
        File tmpFile = Files.createTempFile("ocp-examples-", ".txt").toFile();
        tmpFile.deleteOnExit();
        if (content != null) {
            try (FileOutputStream fileOutput = new FileOutputStream(tmpFile);
                 PrintWriter writer = new PrintWriter(fileOutput)) {
                writer.write(content);
            }
        }
        return tmpFile;
    }

    public static File createTempDir() throws IOException {
        File tmpDir = Files.createTempDirectory("ocp-examples-").toFile();
        tmpDir.deleteOnExit();
        return tmpDir;
    }

    public static String getContentAsString(File file) throws IOException {
        return Files.lines(file.toPath()).collect(Collectors.joining(System.lineSeparator()));
    }
}
