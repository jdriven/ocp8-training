package chapter3.generics;

import java.util.HashMap;
import java.util.Map;

public class GenericsTest {
    public static void main(String[] args) {

        NonGenericNumberCache nonGenericCache = new NonGenericNumberCache();
        nonGenericCache.put("value1", 5L);
        nonGenericCache.put("value2", 10L);
        nonGenericCache.put("value3", 1.23);
        // Long value1 = nonGenericCache.get("value1"); // Does not compile
        Long value1 = (Long) nonGenericCache.get("value1");
        System.out.println(value1);

        GenericNumberCache<Long> genericLongCache = new GenericNumberCache<>();
        genericLongCache.put("value1", 5L);
        genericLongCache.put("value2", 10L);
        // genericLongCache.put("value3", 1.23); // Does not compile, only Longs are accepted
        Long value2 = genericLongCache.get("value2"); // No cast needed
        System.out.println(value2);

        GenericNumberCache<Double> genericDoubleCache = new GenericNumberCache<>();
        genericDoubleCache.put("value1", 1.23);
        genericDoubleCache.put("value2", 4.56);
        Double value = genericDoubleCache.get("value1");
        System.out.println(value);

        System.out.println("Size of genericLongCache: " + getCacheSize(genericLongCache));

        addDoubleToCache(genericDoubleCache);
        System.out.println("Added double to double cache: " + genericDoubleCache.get("doubleValue"));

        GenericNumberCache<Number> numberCache = new GenericNumberCache<>();
        addDoubleToCache(numberCache);
        System.out.println("Added double to number cache: " + numberCache.get("doubleValue"));
    }

    private static int getCacheSize(GenericNumberCache<?> cache) { // We don't care about the cache type
        return cache.getSize();
    }

    private static void addDoubleToCache(GenericCacheInterface<String, ? super Double> cache) {
        cache.put("doubleValue", 1234.5678);
    }
}

class NonGenericNumberCache {
    private final Map<String, Number> internalCache = new HashMap<>();

    public void put(String key, Number value) {
        internalCache.put(key, value);
    }

    public Number get(String key) {
        return internalCache.get(key);
    }
}

class GenericNumberCache<T extends Number> implements GenericCacheInterface<String, T> {
    private final Map<String, T> internalCache = new HashMap<>();

    @Override
    public void put(String key, T value) {
        internalCache.put(key, value);
    }

    @Override
    public T get(String key) {
        return internalCache.get(key);
    }

    public int getSize() {
        return internalCache.size();
    }
}

interface GenericCacheInterface<K, V> {
    void put(K key, V value);

    V get(K key);
}
