package chapter3.collections;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ListTest {
    public static void main(String[] args) {

        List<Integer> arrayList = new ArrayList<>();
        arrayList.add(10);
        arrayList.add(20);
        arrayList.add(20);
        arrayList.add(30);

        System.out.print("ArrayList: ");
        for (Integer item : arrayList) {
            System.out.print(item);
        }
        System.out.println();
        System.out.println("arrayList.contains(10): " + arrayList.contains(10));
        System.out.println("arrayList.indexOf(20): " + arrayList.indexOf(20));
        System.out.println("arrayList.lastIndexOf(20): " + arrayList.lastIndexOf(20));
        arrayList.remove(1);
        arrayList.remove(Integer.valueOf(30));
        System.out.println("ArrayList after removals: " + arrayList);

        System.out.println();

        LinkedList<String> linkedList = new LinkedList<>();
        linkedList.add("A");
        linkedList.add("B");
        linkedList.add("B");
        linkedList.add("C");

        System.out.print("LinkedList: ");
        for (String item : linkedList) {
            System.out.print(item);
        }
    }
}
