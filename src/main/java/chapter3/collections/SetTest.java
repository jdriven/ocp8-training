package chapter3.collections;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class SetTest {
    public static void main(String[] args) {
        Set<String> hashSet = new HashSet<>();
        hashSet.add("1 - Arie");
        hashSet.add("2 - Sjaak");
        hashSet.add("2 - Sjaak");
        hashSet.add("2 - Sjaak");
        hashSet.add("3 - Joop");
        System.out.println("HashSet contents: " + hashSet);

        System.out.println();

        Set<String> treeSet = new TreeSet<>();
        treeSet.add("1 - Arie");
        treeSet.add("2 - Sjaak");
        treeSet.add("3 - Joop");
        System.out.println("TreeSet contents: " + treeSet);

        Set<String> treeSetWithCustomComparator = new TreeSet<>((s1, s2) -> s2.length() - s1.length());
        treeSetWithCustomComparator.add("z");
        treeSetWithCustomComparator.add("yy");
        treeSetWithCustomComparator.add("xxx");
        System.out.println("TreeSet with custom comparator contents: " + treeSetWithCustomComparator);
    }
}
