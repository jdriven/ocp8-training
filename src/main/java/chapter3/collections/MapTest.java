package chapter3.collections;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class MapTest {
    public static void main(String[] args) {

        Map<String, Integer> hashMap = new HashMap<>();
        hashMap.put("three", 3);
        hashMap.put("two", 2);
        hashMap.put("one", 1);
        System.out.println("HashMap contents: " + hashMap);
        System.out.println("hashMap.get(\"one\"): " + hashMap.get("one"));
        System.out.println("hashMap.containsKey(\"two\"): " + hashMap.containsKey("two"));
        System.out.println("hashMap.containsValue(3): " + hashMap.containsValue(3));
        System.out.println("hashMap.keySet(): " + hashMap.keySet());
        System.out.println("hashMap.values(): " + hashMap.values());

        System.out.println("hashMap.putIfAbsent(\"one\", 100): " + hashMap.putIfAbsent("one", 100));
        System.out.println("hashMap.putIfAbsent(\"four\", 4): " + hashMap.putIfAbsent("four", 4));
        System.out.println("hashMap.get(\"four\"): " + hashMap.get("four"));

        System.out.println("hashMap.merge(\"five\", 500, (old, given) -> given / 100): " + hashMap.merge("five", 500, (old, given) -> given / 100));
        System.out.println("hashMap.merge(\"five\", 500, (old, given) -> given / 100): " + hashMap.merge("five", 500, (old, given) -> given / 100));
        System.out.println("hashMap.get(\"five\"): " + hashMap.get("five"));

        System.out.println("hashMap.computeIfPresent(\"one\", (key, value) -> value * 2): " + hashMap.computeIfPresent("one", (key, value) -> value * 2));
        System.out.println("hashMap.get(\"one\"): " + hashMap.get("one"));

        System.out.println("hashMap.computeIfAbsent(\"six\", key -> 6): " + hashMap.computeIfAbsent("six", key -> 6));
        System.out.println("hashMap.get(\"six\"): " + hashMap.get("six"));

        System.out.println();

        TreeMap<Long, String> treeMap = new TreeMap<>();
        treeMap.put(3L, "three");
        treeMap.put(2L, "two");
        treeMap.put(1L, "one");
        System.out.println("TreeMap contents: " + treeMap);
    }
}
