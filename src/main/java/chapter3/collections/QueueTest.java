package chapter3.collections;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Queue;

public class QueueTest {
    public static void main(String[] args) {

        Queue<String> linkedList = new LinkedList<>(); // Note: LinkedList is also a Deque
        linkedList.offer("First");
        linkedList.offer("Second");
        linkedList.offer("Third");
        System.out.println("linkedList.peek(): " + linkedList.peek());
        System.out.println("linkedList.peek(): " + linkedList.peek());
        System.out.println("linkedList.poll(): " + linkedList.poll());
        System.out.println("linkedList.remove(): " + linkedList.remove());
        System.out.println("LinkedList contents: " + linkedList);

        Deque<String> arrayDeque = new ArrayDeque<>();
        arrayDeque.offer("First");
        arrayDeque.offer("Second");
        arrayDeque.offer("Third");
        System.out.println("arrayDeque.pollFirst(): " + arrayDeque.pollFirst());
        System.out.println("arrayDeque.pollLast(): " + arrayDeque.pollLast());
        System.out.println("arrayDeque contents: " + arrayDeque);
        arrayDeque.push("New first");
        System.out.println("ArrayDeque contents after push: " + arrayDeque);
    }
}
