package chapter3.quiz;

import java.util.ArrayList;
import java.util.List;

public class Question1 {

    public static <E extends CharSequence> List<? super E> doIt(List<E> nums) {
        return nums;
    }

    private void answer1() {
        ArrayList<String> in = new ArrayList<>();
        List<CharSequence> result;
        // result = doIt(in);
    }

    private void answer2() {
        List<String> in = new ArrayList<>();
        List<Object> result;
        // result = doIt(in);
    }

    private void answer3() {
        ArrayList<String> in = new ArrayList<>();
        List result;
        // result = doIt(in);
    }

    private void answer4() {
        ArrayList<CharSequence> in = new ArrayList<>();
        List<CharSequence> result;
        // result = doIt(in);
    }

    private void answer5() {
        ArrayList<Object> in = new ArrayList<>();
        List<CharSequence> result;
        // result = doIt(in);
    }
}
