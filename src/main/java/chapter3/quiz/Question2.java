package chapter3.quiz;

public class Question2 {
}

class Game{}
class Cricket extends Game{}
class Instrument{}
class Guitar extends Instrument{}

interface Player<E>{ void play(E e); }
interface GamePlayer<E extends Game> extends Player<E>{}
interface MusicPlayer<E extends Instrument> extends Player{}

// Answer 1
// class Batsman implements GamePlayer<Cricket> {
//     public void play(Game o){ }
// }

// Answer 2
// class Bowler implements GamePlayer<Guitar>{
//     public void play(Game o){ }
// }

// Answer 3
// class Bowler implements Player<Guitar> {
//     public void play(Guitar o) { }
// }

// Answer 4
// class MidiPlayer implements MusicPlayer {
//     public void play(Guitar o) { }
// }

// Answer 5
// class MidiPlayer implements MusicPlayer<Instrument> {
//     public void play (Guitar o) { }
// }
