package chapter3.ordering;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class OrderingStrings {
    public static void main(String[] args) {
        List<String> strings = new ArrayList<>();
        strings.add("z");
        strings.add("A");
        strings.add("Z");
        strings.add("a");
        strings.add("1");
        strings.add("9");
        Collections.sort(strings);
        System.out.println("Sorted strings: " + strings);
    }
}
