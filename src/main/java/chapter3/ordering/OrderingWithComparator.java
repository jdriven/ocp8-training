package chapter3.ordering;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;

public class OrderingWithComparator {
    public static void main(String[] args) {
        Set<MyData> myDataSet = new TreeSet<>(new MyDataComparator());
        myDataSet.add(new MyData(5));
        myDataSet.add(new MyData(3));
        myDataSet.add(new MyData(7));
        myDataSet.add(new MyData(9));
        myDataSet.add(new MyData(1));
        System.out.println("My data set: " + myDataSet);

        List<MyData> myDataList = new ArrayList<>(myDataSet);
        Comparator<MyData> myDataListComparator = (md1, md2) -> md2.getId() - md1.getId();
        myDataList.sort(myDataListComparator);
        System.out.println("My custom sorted data list: " + myDataList);

        int binarySearchFor3 = Collections.binarySearch(myDataList, new MyData(3), myDataListComparator);
        System.out.println("Binary search for id=3, index: " + binarySearchFor3);

        int binarySearchWithWrongComparator = Collections.binarySearch(myDataList, new MyData(3), new MyDataComparator());
        System.out.println("Binary search for id=3 with wrong comparator, index: " + binarySearchWithWrongComparator);
    }
}

class MyDataComparator implements Comparator<MyData> {
    @Override
    public int compare(MyData o1, MyData o2) {
        return o1.getId() - o2.getId();
    }
}

class MyData {

    private int id;

    public MyData(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MyData)) return false;
        return getId() == ((MyData) o).getId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

    @Override
    public String toString() {
        return String.format("[id=%d]", id);
    }
}
