package chapter3.ordering;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;

public class OrderingComparables {
    public static void main(String[] args) {
        Set<MyDataWithComparator> myDataSet = new TreeSet<>();
        myDataSet.add(new MyDataWithComparator(5));
        myDataSet.add(new MyDataWithComparator(3));
        myDataSet.add(new MyDataWithComparator(7));
        myDataSet.add(new MyDataWithComparator(9));
        myDataSet.add(new MyDataWithComparator(1));
        System.out.println("My data set: " + myDataSet);

        List<MyDataWithComparator> myDataList = new ArrayList<>(myDataSet);
        int binarySearchFor5 = Collections.binarySearch(myDataList, new MyDataWithComparator(5));
        System.out.println("Binary search for id=5, index: " + binarySearchFor5);
    }
}

class MyDataWithComparator implements Comparable<MyDataWithComparator> {

    private int id;

    public MyDataWithComparator(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    @Override
    public int compareTo(MyDataWithComparator o) {
        return this.id - o.id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MyDataWithComparator)) return false;
        return getId() == ((MyDataWithComparator) o).getId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

    @Override
    public String toString() {
        return String.format("[id=%d]", id);
    }
}
