package chapter6;

import java.io.IOException;

public class CodeExecutionOrder {
    public static void main(String[] args) {
        executionOrderWhenTryBlockThrowsException();
        System.out.println();
        executionOrderWhenTryBlockExecutesNormally();
    }

    private static void executionOrderWhenTryBlockThrowsException() {
        System.out.println("Execution order when try block throws exception:");

        try (SomeAutoCloseable someAutoCloseable = new SomeAutoCloseable()) {
            System.out.println("main - Executing try block");
            throw new IOException("main - exception");
        } catch (IOException e) {
            System.out.println("main - Caught exception from try block, suppressed exception: " + e.getSuppressed()[0]);
        } catch (IllegalStateException e) {
            System.out.println("main - Caught SomeAutoCloseable exception");
        } finally {
            System.out.println("main - finally");
        }
    }

    private static void executionOrderWhenTryBlockExecutesNormally() {
        System.out.println("Execution order when try block executes normally:");

        try (SomeAutoCloseable someAutoCloseable = new SomeAutoCloseable()) {
            System.out.println("main - Executing try block");
        } catch (IllegalStateException e) {
            System.out.println("main - Caught SomeAutoCloseable exception");
        } finally {
            System.out.println("main - finally");
        }
    }
}

class SomeAutoCloseable implements AutoCloseable {

    SomeAutoCloseable() {
        System.out.println("SomeAutoCloseable - constructor");
    }

    @Override
    public void close() throws IllegalStateException {
        System.out.println("SomeAutoCloseable - close");
        throw new IllegalStateException("SomeAutocloseable - exception");
    }
}
