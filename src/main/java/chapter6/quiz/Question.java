package chapter6.quiz;

import java.io.IOException;
import java.sql.Connection;

public class Question {
    public static void main(String[] args) throws Exception {
        new Question().readData("filename");
    }

    public void readData(String fileName) throws Exception {
        try(FileReader fr1 = new FileReader(fileName)) {
            Connection c = getConnection();
            //...other valid code
        }
    }

    private Connection getConnection() throws ClassNotFoundException {
        throw new ClassNotFoundException("Test");
    }

    private static class FileReader implements AutoCloseable {

        private String name;

        FileReader(String name) {
            this.name = name;
        }

        @Override
        public void close() throws IOException {
            throw new IOException(name);
        }
    }
}
