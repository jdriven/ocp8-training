package chapter6.multicatch;

import java.io.FileNotFoundException;
import java.io.IOException;

public class MultiCatchExample {
    public static void main(String[] args) {
        doMultiCatch(new IOException("IOException"));
        doMultiCatch(new FileNotFoundException("FileNotFoundException")); // Subclass of IOException!
        doMultiCatch(new IllegalStateException("IllegalStateException"));
        doMultiCatch(new UnsupportedOperationException("UnsupportedOperationException"));
    }

    private static void doMultiCatch(Exception e) {
        try {
            throw e;
        } catch (IOException | IllegalStateException e1) {
            System.out.println("Caught IOException or IllegalStateException with message: " + e1.getMessage());
        } catch (Exception e2) {
            System.out.println("Caught general Exception with message: " + e2.getMessage());
        }
    }
}
