package chapter6.trywithresources;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;

public class TryWithResourcesExample {
    public static void main(String[] args) {

        try (InputStream resourceInput = TryWithResourcesExample.class.getResourceAsStream("textfile.txt");
             BufferedInputStream bufferedInputStream = new BufferedInputStream(resourceInput);
             StringWriter writer = new StringWriter()) {

            int input;
            while ((input = bufferedInputStream.read()) != -1) {
                writer.write(input);
            }

            System.out.println("Read String: " + writer);

        } catch (IOException e) {
            System.out.println("Caught IOException with message: " + e.getMessage());
        }

        // No need to close the streams
    }
}
