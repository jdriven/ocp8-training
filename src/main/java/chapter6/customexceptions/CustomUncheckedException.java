package chapter6.customexceptions;

public class CustomUncheckedException extends RuntimeException {
    public CustomUncheckedException(String msg) {
        super(msg);
    }
}

class CustomUncheckedExceptionExample {
    public static void main(String[] args) {

        try {
            someMethod();
        } catch (CustomUncheckedException e) {
            System.out.println("Caught CustomUncheckedException with message: " + e.getMessage());
        }

        someMethod(); // No need to use try-catch
    }

    private static void someMethod() throws CustomUncheckedException {
        someNestedMethod(); // No need to use try-catch, because of the 'throws' in the declaration
    }

    private static void someNestedMethod() throws CustomUncheckedException {
        throw new CustomUncheckedException("test");
    }
}
