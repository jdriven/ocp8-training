package chapter6.customexceptions;

public class CustomCheckedException extends Exception {
    public CustomCheckedException(String msg) {
        super(msg);
    }
}

class CustomCheckedExceptionExample {
    public static void main(String[] args) {

        try {
            someMethod();
        } catch (CustomCheckedException e) {
            System.out.println("Caught CustomCheckedException with message: " + e.getMessage());
        }

        // someMethod(); // Does not compile, unhandled exception!
    }

    private static void someMethod() throws CustomCheckedException {
        someNestedMethod(); // No need to use try-catch, because of the 'throws' in the declaration
    }

    private static void someNestedMethod() throws CustomCheckedException {
        throw new CustomCheckedException("test");
    }
}
