package chapter2.polymorphism;

public class PolymorphismTest {
    public static void main(String[] args) {
        Animal buffalo = new Buffalo();
        Animal lion = new Lion();

        feed(buffalo);
        feed(lion);
    }

    private static void feed(Animal animal) {
        animal.eat();
    }
}

interface Animal {
    void eat();
}

class Buffalo implements Animal {

    @Override
    public void eat() {
        System.out.println("Eating grass...");
    }
}

class Lion implements Animal {

    @Override
    public void eat() {
        System.out.println("Eating buffalo...");
    }
}
