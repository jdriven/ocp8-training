package chapter2.quiz;

public class Question3 {
    public static void main(String[] args) {
        // A1 a1 = () -> System.out.println("A1");
        // A2 a2 = () -> System.out.println("A2");
        // A3 a3 = () -> System.out.println("A3");
        // A4 a4 = () -> System.out.println("A4");
        // A5 a5 = () -> System.out.println("A5");
    }
}

// interface A1{
// }
//
// interface A2{
//     default void m(){};
// }
//
// interface A3{
//     void m(){};
// }
//
// interface A4{
//     default void m1() {};
//     void m2();
// }
//
// interface A5{
//     void m1();
//     void m2();
// }
