package chapter2.casting;

@SuppressWarnings({"RedundantCast", "CastCanBeRemovedNarrowingVariableType", "ConstantConditions"})
public class CastingTest {
    public static void main(String[] args) {
        Number number = 42L;
        System.out.println("casted number to Object: " + ((Object) number));
        System.out.println("casted number to Long: " + ((Long) number));
        // System.out.println("casted number to String: " + ((String) number)); // Won't compile

        InterfaceA instanceOfA = new InterfaceA() {};
        System.out.println("casted instanceOfA to InterfaceB: " + ((InterfaceB) instanceOfA)); // Compiles, but throws exception!

        ImplementationX implementationX = new ImplementationX();
        System.out.println("casted implementationX to InterfaceA: " + ((InterfaceA) implementationX));
        System.out.println("casted implementationX to InterfaceB: " + ((InterfaceB) implementationX));

        System.out.println("cast null to String: " + ((String) null));

        // print(null); // Won't compile, ambiguous call!
        print((String) null);
        print((Integer) null);
    }

    private static void print(String s) {
        System.out.println("String: " + s);
    }

    private static void print(Integer i) {
        System.out.println("Integer: " + i);
    }

    private interface InterfaceA {}
    private interface InterfaceB {}
    private static class ImplementationX implements InterfaceA, InterfaceB {}
}
