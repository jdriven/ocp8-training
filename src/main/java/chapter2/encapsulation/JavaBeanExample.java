package chapter2.encapsulation;

import java.util.Objects;

public class JavaBeanExample {

    private int id;

    private boolean active = true;

    private Boolean locked;

    private String description = "unknown";

    public int getId() {
        return id;
    }

    public void setId(int id) {
        if (id < 0) {
            throw new IllegalArgumentException("id should be positive!");
        }
        this.id = id;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Boolean getLocked() {
        return locked;
    }

    public void setLocked(Boolean locked) {
        this.locked = locked;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        if (description == null || description.trim().isEmpty()) {
            throw new IllegalArgumentException("Description should not be empty!");
        }
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof JavaBeanExample)) return false;
        return getId() == ((JavaBeanExample) o).getId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
