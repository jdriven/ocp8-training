package chapter2.functional_interface;

@SuppressWarnings({"Convert2Lambda", "Convert2MethodRef"})
public class FunctionalInterfaceTest {
    public static void main(String[] args) {

        Calculator multiplier = new Calculator() {
            @Override
            public int calculate(int i1, int i2) {
                return i1 * i2;
            }
        };

        System.out.println("2 * 4 = " + multiplier.calculate(2, 4));

        Calculator adder = (a, b) -> a + b;

        System.out.println("40 + 2 = " + adder.calculate(40, 2));
    }
}

@FunctionalInterface
interface Calculator {

    int calculate(int i1, int i2);

}
