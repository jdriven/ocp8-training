package chapter2.design_patterns;

public class BuilderTest {
    public static void main(String[] args) {

        // MyObject myObject = new MyObject(1, "test"); // Compile error!

        MyObject myObject = new MyObject.Builder()
                .withId(1)
                .withDescription("desc")
                .build();

        System.out.println("id: " + myObject.getId());
        System.out.println("description: " + myObject.getDescription());

        MyObject myObject2 = new MyObject.Builder()
                .withId(-123)
                .build(); // Throws exception!
    }
}

final class MyObject {

    private final int id;
    private final String description;

    private MyObject(int id, String description) {
        this.id = id;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public static class Builder {
        private int id = -1;
        private String description = "unknown";

        public Builder withId(int id) {
            this.id = id;
            return this;
        }

        public Builder withDescription(String description) {
            this.description = description;
            return this;
        }

        public MyObject build() {
            if (id < 0) {
                throw new IllegalArgumentException("id should be positive!");
            }
            if (description == null || description.trim().isEmpty()) {
                throw new IllegalArgumentException("description should not be empty!");
            }
            return new MyObject(id, description);
        }
    }
}
