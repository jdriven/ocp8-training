package chapter2.design_patterns;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ImmutableObjectTest {
    public static void main(String[] args) {
        ImmutableObject obj = new ImmutableObject("some description", Arrays.asList("a", "b", "c"));
        System.out.println("Description: " + obj.getDescription());
        System.out.println("Values: " + obj.getValues());

        ImmutableObject newObj = obj.withDescription("new description");
        System.out.println("New description: " + newObj.getDescription());
        System.out.println("New values: " + newObj.getValues());

        obj.getValues().remove(0); // Throws exception
    }
}

final class ImmutableObject {

    private final String description;

    private final List<String> values;

    public ImmutableObject(String description, List<String> values) {
        this.description = description;
        this.values = new ArrayList<>(values);
    }

    public String getDescription() {
        return description;
    }

    public List<String> getValues() {
        return Collections.unmodifiableList(values);
    }

    public ImmutableObject withDescription(String newDescription) {
        return new ImmutableObject(newDescription, values);
    }

    public ImmutableObject withValues(List<String> newValues) {
        return new ImmutableObject(description, newValues);
    }
}
