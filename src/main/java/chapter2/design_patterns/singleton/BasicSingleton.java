package chapter2.design_patterns.singleton;

public final class BasicSingleton {

    private int counter = 0;

    private static final BasicSingleton INSTANCE = new BasicSingleton();

    private BasicSingleton() { }

    public static BasicSingleton getInstance() {
        return INSTANCE;
    }

    public void increment() {
        counter++;
    }

    public int getCounter() {
        return counter;
    }
}

@SuppressWarnings("ConstantConditions")
class BasicSingletonTest {
    public static void main(String[] args) {

        // BasicSingleton instance = new BasicSingleton(); // Compile error!

        BasicSingleton instance1 = BasicSingleton.getInstance();
        instance1.increment();

        BasicSingleton instance2 = BasicSingleton.getInstance();
        System.out.println("instance1 == instance2? " + (instance1 == instance2));
        instance2.increment();
        System.out.println("instance1 counter: " + instance1.getCounter());
        System.out.println("instance2 counter: " + instance2.getCounter());
    }
}
