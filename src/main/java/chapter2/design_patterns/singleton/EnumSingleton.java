package chapter2.design_patterns.singleton;

public enum EnumSingleton {
    INSTANCE;

    private int counter = 0;

    public void increment() {
        counter++;
    }

    public int getCounter() {
        return counter;
    }
}

@SuppressWarnings("ConstantConditions")
class EnumSingletonTest {
    public static void main(String[] args) {
        EnumSingleton instance1 = EnumSingleton.INSTANCE;
        instance1.increment();

        EnumSingleton instance2 = EnumSingleton.INSTANCE;
        System.out.println("instance1 == instance2? " + (instance1 == instance2));
        instance2.increment();
        System.out.println("instance1 counter: " + instance1.getCounter());
        System.out.println("instance2 counter: " + instance2.getCounter());
    }
}

