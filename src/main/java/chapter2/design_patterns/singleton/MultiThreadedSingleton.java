package chapter2.design_patterns.singleton;

public final class MultiThreadedSingleton {

    private int counter = 0;

    private static volatile MultiThreadedSingleton instance;

    private MultiThreadedSingleton() { }

    public static MultiThreadedSingleton getInstance() {
        if (instance == null) {
            synchronized (MultiThreadedSingleton.class) {
                if (instance == null) {
                    instance = new MultiThreadedSingleton();
                }
            }
        }
        return instance;
    }

    public synchronized void increment() {
        counter++;
    }

    public synchronized int getCounter() {
        return counter;
    }
}

class MultiThreadedSingletonTest {
    public static void main(String[] args) {

        // MultiThreadedSingleton instance = new MultiThreadedSingleton(); // Compile error!

        MultiThreadedSingleton instance1 = MultiThreadedSingleton.getInstance();
        instance1.increment();

        MultiThreadedSingleton instance2 = MultiThreadedSingleton.getInstance();
        System.out.println("instance1 == instance2? " + (instance1 == instance2));
        instance2.increment();
        System.out.println("instance1 counter: " + instance1.getCounter());
        System.out.println("instance2 counter: " + instance2.getCounter());
    }
}
