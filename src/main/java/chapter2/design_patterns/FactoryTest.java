package chapter2.design_patterns;

public class FactoryTest {
    public static void main(String[] args) {
        System.out.println("Budget of 200000: " + CarFactory.create(200_000).getBrand());
        System.out.println("Budget of 75000: " + CarFactory.create(75_000).getBrand());
        System.out.println("Budget of 25000: " + CarFactory.create(25_000).getBrand());
        System.out.println("Budget of 1000: " + CarFactory.create(1000).getBrand());
    }
}

interface Car {
    String getBrand();
}

class Ferrari implements Car {
    @Override
    public String getBrand() {
        return "Ferrari";
    }
}

class Mercedes implements Car {
    @Override
    public String getBrand() {
        return "Mercedes";
    }
}

class Ford implements Car {
    @Override
    public String getBrand() {
        return "Ford";
    }
}

final class CarFactory {

    public static Car create(int budget) {
        if (budget > 100_000) {
            return new Ferrari();
        }
        if (budget > 50_000) {
            return new Mercedes();
        }
        if (budget > 5_000) {
            return new Ford();
        }
        throw new IllegalArgumentException("Budget too low!");
    }
}
