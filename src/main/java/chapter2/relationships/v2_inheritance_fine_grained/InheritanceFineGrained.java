package chapter2.relationships.v2_inheritance_fine_grained;

public class InheritanceFineGrained {
    public static void main(String[] args) {
        HomeKitchen homeKitchen = new HomeKitchenImpl();
        homeKitchen.makeCoffee();
        homeKitchen.makeDinner();

        OfficePantry officePantry = new OfficePantryImpl();
        officePantry.makeCoffee();
        // officePantry.makeDinner(); // Compile error!
    }
}

interface CoffeeMaker {
    void makeCoffee();
}

interface Stove {
    void makeDinner();
}

interface HomeKitchen extends CoffeeMaker, Stove { }

interface OfficePantry extends CoffeeMaker { }

class HomeKitchenImpl implements HomeKitchen {

    @Override
    public void makeCoffee() {
        System.out.println("Making coffee...");
    }

    @Override
    public void makeDinner() {
        System.out.println("Making dinner...");
    }
}

class OfficePantryImpl implements OfficePantry {

    @Override
    public void makeCoffee() {
        System.out.println("Making coffee...");
    }
}
