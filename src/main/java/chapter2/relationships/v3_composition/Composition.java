package chapter2.relationships.v3_composition;

public class Composition {
    public static void main(String[] args) {
        HomeKitchen homeKitchen = new HomeKitchen();
        homeKitchen.makeCoffee();
        homeKitchen.makeDinner();

        OfficePantry officePantry = new OfficePantry();
        officePantry.makeCoffee();
        // officePantry.makeDinner(); // Compile error!
    }
}

class CoffeeMaker {
    void makeCoffee() {
        System.out.println("Making coffee...");
    }
}

class Stove {
    void makeDinner() {
        System.out.println("Making dinner...");
    }
}

class HomeKitchen {
    private CoffeeMaker coffeeMaker = new CoffeeMaker();
    private Stove stove = new Stove();

    void makeCoffee() {
        coffeeMaker.makeCoffee();
    }

    void makeDinner() {
        stove.makeDinner();
    }
}

class OfficePantry {
    private CoffeeMaker coffeeMaker = new CoffeeMaker();

    void makeCoffee() {
        coffeeMaker.makeCoffee();
    }
}
