package chapter2.relationships.v1_inheritance;

public class Inheritance {
    public static void main(String[] args) {
        Kitchen homeKitchen = new HomeKitchen();
        homeKitchen.makeCoffee();
        homeKitchen.makeDinner();

        Kitchen officePantry = new OfficePantry();
        officePantry.makeCoffee();
        officePantry.makeDinner(); // Error!
    }
}

interface Kitchen {

    void makeCoffee();

    void makeDinner();
}

class HomeKitchen implements Kitchen {

    @Override
    public void makeCoffee() {
        System.out.println("Making coffee...");
    }

    @Override
    public void makeDinner() {
        System.out.println("Making dinner...");
    }
}

class OfficePantry implements Kitchen {

    @Override
    public void makeCoffee() {
        System.out.println("Making coffee...");
    }

    @Override
    public void makeDinner() {
        throw new UnsupportedOperationException("Not really suitable for dinners...");
    }
}
