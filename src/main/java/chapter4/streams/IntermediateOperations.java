package chapter4.streams;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SuppressWarnings({"ResultOfMethodCallIgnored", "Convert2MethodRef"})
public class IntermediateOperations {
    public static void main(String[] args) {
        filter();
        System.out.println();
        map();
        System.out.println();
        flatMap();
        System.out.println();
        distinct();
        System.out.println();
        limit();
        System.out.println();
        skip();
        System.out.println();
        sorted();
        System.out.println();
        peek();
    }

    private static void filter() {
        System.out.println("filter():");

        Stream<Integer> numbers = Stream.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

        List<Integer> evenNumbers = numbers
                .filter(IntermediateOperations::isEven)
                .collect(Collectors.toList());
        System.out.println(evenNumbers);
    }

    private static boolean isEven(int number) {
        return number % 2 == 0;
    }

    private static void map() {
        System.out.println("map():");

        Stream<String> strings = Stream.of("a", "bb", "ccc", "dddd");

        List<Integer> lengths = strings
                .map(String::length)
                .collect(Collectors.toList());
        System.out.println(lengths);
    }

    private static void flatMap() {
        System.out.println("flatMap():");

        List<List<String>> listOfListsOfLetters = Arrays.asList(
                Arrays.asList("a", "a", "a"),
                Arrays.asList("b", "b", "b"),
                Arrays.asList("c", "c", "c")
        );

        List<String> letters = listOfListsOfLetters.stream()
                .flatMap(listOfLetters -> listOfLetters.stream()) // Or: .flatMap(Collection::stream)
                .collect(Collectors.toList());

        System.out.println(letters);
    }

    private static void distinct() {
        System.out.println("distinct():");
        Stream<String> letters = Stream.of("o", "c", "p", "c", "o", "u", "r", "s", "e");

        List<String> distinctLetters = letters
                .distinct()
                .collect(Collectors.toList());
        System.out.println(distinctLetters);
    }

    private static void limit() {
        System.out.println("limit():");
        Random random = new Random();
        Stream<Integer> infiniteRandomNumbers = Stream.generate(random::nextInt);

        // Without a limit, this infinite stream can never be fully collected into a list
        List<Integer> threeRandomNumbers = infiniteRandomNumbers
                .limit(3)
                .collect(Collectors.toList());
        System.out.println(threeRandomNumbers);
    }

    private static void skip() {
        System.out.println("skip():");
        Stream<Integer> numbers = Stream.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

        List<Integer> lastNumbers = numbers
                .skip(5)
                .collect(Collectors.toList());
        System.out.println(lastNumbers);
    }

    private static void sorted() {
        System.out.println("sorted():");

        Stream<Integer> numbers = Stream.of(5, 3, 6, 8, 93, 6, 7);
        List<Integer> sortedAscending = numbers
                .sorted()
                .collect(Collectors.toList());
        System.out.println(sortedAscending);

        Stream<String> strings = Stream.of("x", "xxx", "xx");
        List<String> sortedByLength = strings
                .sorted(Comparator.comparingInt(String::length))
                .collect(Collectors.toList());
        System.out.println(sortedByLength);
    }

    private static void peek() {
        System.out.println("peek():");
        Stream<String> strings = Stream.of("a", "b", "c");

        strings
                .peek(System.out::println)
                .collect(Collectors.toList());

    }
}
