package chapter4.streams;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Stream;

@SuppressWarnings("unused")
public class CreatingStreams {
    public static void main(String[] args) {
        Stream<?> emptyStream = Stream.empty();

        Stream<Integer> constantStream = Stream.of(1, 2, 3);

        List<String> strings = Arrays.asList("a", "b", "c");
        Stream<String> stringStream = strings.stream();

        Random random = new Random();
        Stream<Integer> randomStream = Stream.generate(random::nextInt);
    }
}
