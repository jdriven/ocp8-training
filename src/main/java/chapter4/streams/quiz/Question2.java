package chapter4.streams.quiz;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

class Course {
    private String id;
    private String name;

    public Course(String id, String name) {
        this.id = id; this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}

public class Question2 {
    public static void main(String[] args) {
        List<Course> cList = Arrays.asList(
            new Course("803", "OCAJP7"),
            new Course("808", "OCAJP8"),
            new Course("809", "OCPJP8")
        );

        cList.stream().filter(c -> c.getName().indexOf("8") > -1)
            .map(c -> c.getId())
            .collect(Collectors.joining("1Z0-"));
        cList.stream().forEach(c -> System.out.println(c.getId()));
    }
}

// What will the code above print?
//
//      883
// 1:   808
//      809
//
//      803
// 2:   1Z0-808
//      1Z0-809
//
// 3:   1Z0-808
//      1Z0-809
//
// 4:   It will throw an exception at runtime.
//
// 5:   It will not compile

