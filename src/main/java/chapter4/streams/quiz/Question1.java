package chapter4.streams.quiz;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Question1 {
}

class Names {
    private List<String> list;
    public List<String> getList() {
        return list;
    }
    public void setList(List<String> list) {
        this.list = list;
    }
    public void printNames() {
        System.out.println(getList());
    }

    public static void main(String[] args) {
//        List<String> list = Arrays.asList(
//            "Bob Hope",
//            "Bob Dole",
//            "Bob Brown"
//        );
//        Names n = new Names();
//        n.setList(list.stream().collect(Collectors.toList()));
//        n.getList().forEach(Names::printNames);
    }

}

//    What will the code print when compiled and run?
//
//    1: [Bob Hope, Bob Dole, Bob Brown]
//
//    2: Bob HopeBob DoleBobBrown
//
//    3: [null, null, null]
//
//    4: Compilation error
//
//    5: An exception will be thrown at runtime.




