package chapter4.streams;

import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

@SuppressWarnings("SimplifyStreamApiCallChains")
public class TerminalOperations {
    public static void main(String[] args) {
        count();
        System.out.println();
        min();
        System.out.println();
        max();
        System.out.println();
        findAny();
        System.out.println();
        findFirst();
        System.out.println();
        allMatch();
        System.out.println();
        anyMatch();
        System.out.println();
        noneMatch();
        System.out.println();
        forEach();
        System.out.println();
        reduce();
        System.out.println();
        collect();
    }

    private static void count() {
        System.out.println("count():");

        Stream<String> words = Stream.of("some", "words", "in", "a", "stream");
        System.out.println(words.count());
    }

    private static void min() {
        System.out.println("min():");

        Stream<Integer> numbers = Stream.of(1, 2, 3, 4, 5);
        System.out.println(numbers.min(Comparator.naturalOrder()).orElse(-1));

        Stream<Integer> emptyStream = Stream.empty();
        System.out.println(emptyStream.min(Comparator.naturalOrder()).orElse(-1));
    }

    private static void max() {
        System.out.println("max():");

        Stream<Integer> numbers = Stream.of(1, 2, 3, 4, 5);
        System.out.println(numbers.max(Comparator.naturalOrder()).orElse(-1));

        Stream<Integer> emptyStream = Stream.empty();
        System.out.println(emptyStream.max(Comparator.naturalOrder()).orElse(-1));
    }

    private static void findAny() {
        System.out.println("findAny():");

        List<Integer> largeList = IntStream.rangeClosed(1, 1_000).boxed().collect(Collectors.toList());

        Integer anyEvenNumber = largeList
                .parallelStream()
                .filter(i -> i % 2 == 0)
                .findAny()
                .orElse(-1);

        System.out.println(anyEvenNumber);
    }

    private static void findFirst() {
        System.out.println("findFirst():");

        List<Integer> largeList = IntStream.rangeClosed(1, 1_000).boxed().collect(Collectors.toList());

        Integer firstEvenNumber = largeList
                .parallelStream()
                .filter(i -> i % 2 == 0)
                .findFirst()
                .orElse(-1);

        System.out.println(firstEvenNumber);
    }

    private static void allMatch() {
        System.out.println("allMatch():");

        Stream<Integer> allEvenNumbers = Stream.of(2, 4, 6, 8);
        boolean allEvenNumbersResult = allEvenNumbers
                .peek(System.out::print)
                .allMatch(i -> i % 2 == 0);
        System.out.println();
        System.out.println(allEvenNumbersResult);

        Stream<Integer> mostlyEvenNumbers = Stream.of(2, 4, 5, 8);
        boolean mostlyEvenNumbersResult = mostlyEvenNumbers
                .peek(System.out::print)
                .allMatch(i -> i % 2 == 0);
        System.out.println();
        System.out.println(mostlyEvenNumbersResult);
    }

    private static void anyMatch() {
        System.out.println("anyMatch():");

        Stream<Integer> allEvenNumbers = Stream.of(2, 4, 6, 8);
        boolean anyEvenNumbersResult = allEvenNumbers
                .peek(System.out::print)
                .anyMatch(i -> i % 2 == 0);
        System.out.println();
        System.out.println(anyEvenNumbersResult);

        Stream<Integer> mostlyOddNumbers = Stream.of(1, 3, 4, 7);
        boolean mostlyOddNumbersResult = mostlyOddNumbers
                .peek(System.out::print)
                .anyMatch(i -> i % 2 == 0);
        System.out.println();
        System.out.println(mostlyOddNumbersResult);
    }

    private static void noneMatch() {
        System.out.println("noneMatch():");

        Stream<Integer> allOddNumbers = Stream.of(1, 3, 5, 7);
        boolean noneEvenNumbersResult = allOddNumbers
                .peek(System.out::print)
                .noneMatch(i -> i % 2 == 0);
        System.out.println();
        System.out.println(noneEvenNumbersResult);

        Stream<Integer> mostlyOddNumbers = Stream.of(1, 3, 4, 7);
        boolean mostlyOddNumbersResult = mostlyOddNumbers
                .peek(System.out::print)
                .noneMatch(i -> i % 2 == 0);
        System.out.println();
        System.out.println(mostlyOddNumbersResult);
    }

    private static void forEach() {
        System.out.println("forEach():");

        Stream.of("a", "bb", "ccc")
                .forEach(s -> System.out.printf("- %s (%d)%n", s, s.length()));
    }

    private static void reduce() {
        System.out.println("reduce():");

        List<String> letters = Arrays.asList("a", "bb", "ccc", "dddd");

        System.out.println(letters.stream().reduce(String::concat).orElse(""));
        System.out.println(letters.stream().reduce("joined: ", String::concat));

        Integer letterCount = letters.stream()
                .reduce(0, (count, l) -> count + l.length(), Integer::sum);
        System.out.println("count: " + letterCount);
    }

    private static void collect() {
        List<String> letters = Arrays.asList("a", "b", "cc", "dd");

        System.out.println(letters.stream().collect(Collectors.toList()));
        System.out.println(letters.stream().collect(
                (Supplier<LinkedList<String>>) LinkedList::new,
                LinkedList::add,
                LinkedList::addAll
        ));

        Map<Integer, List<String>> groupedByLength = letters.stream().collect(Collectors.groupingBy(String::length));
        System.out.println("grouped by length: " + groupedByLength);

        Map<Integer, Long> groupedByCounting = letters.stream().collect(Collectors.groupingBy(String::length, Collectors.counting()));
        System.out.println("grouped by length, counting: " + groupedByCounting);

        // The following line throws an IllegalStateException because of a duplicate key
        // Map<Integer, String> mappedByLength = letters.stream().collect(Collectors.toMap(String::length, Function.identity()));
        Map<Integer, String> mappedByLength = letters.stream().collect(Collectors.toMap(String::length, Function.identity(), (s1, s2) -> s1 + s2));
        System.out.println("mapped by length: " + mappedByLength);

        Stream<Integer> numbers = Stream.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

        Map<Boolean, List<Integer>> evenOrOdd = numbers.collect(Collectors.partitioningBy(i -> i % 2 == 0));
        System.out.println("evenOrOdd: " + evenOrOdd);
    }
}
