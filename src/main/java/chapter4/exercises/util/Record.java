package chapter4.exercises.util;

import java.util.Objects;

public class Record {
    private final String topic;
    private final String content;

    public Record(String topic, String content) {
        this.topic = topic;
        this.content = content;
    }

    public String getTopic() {
        return this.topic;
    }

    public String getContent() {
        return this.content;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Record record = (Record) o;
        return Objects.equals(topic, record.topic) &&
                Objects.equals(content, record.content);
    }

    @Override
    public int hashCode() {
        return Objects.hash(topic, content);
    }
}
