package chapter4.exercises;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;


import chapter4.exercises.util.Person;
import chapter4.exercises.util.Record;
import chapter4.exercises.util.TODO;

public class Exercise006Misc {
    private static boolean isAdult(Person person) {
        return person.getAge() >= 18;
    }

    /**
     Use stream().allMatch and the above method to check if all persons are adult.
     */
    public boolean allAdults(Collection<Person> persons) {
        return TODO.implementMe();
    }

    /**
     Use .filter to return the list of input persons whose name has the input character
     as first letter.
     Write your own lambda function to use with filter.
     */
    public List<Person> nameStartsWith(Collection<Person> persons, char c) {
        return TODO.implementMe();
    }

    /**
     Use Collectors.groupingBy to return a map where containing each possible record topic as key,
     for each key as value a list of all records that have the key value as topic
     */
    public Map<String, List<Record>> byTopic(Collection<Record> records) {
        return TODO.implementMe();
    }

    /**
     Helper function for the next exercise.
     */
    private Record shortestContent(Collection<Record> records) {
        return records.stream().min(Comparator.comparingInt(record -> record.getContent().length())).orElse(null);
    }


    /**
     Let's say for some web app, We want to show a single sample for each topic.
     Make use of the previous exercise byTopic, the shortestContent helper function, and use Collectors.toMap.
     */
    public Map<String, Record> shortestPerTopic(Collection<Record> records) {
        return TODO.implementMe();
    }

    /**
     Return a map containing the key-value pairs
     "shortest", shortest of all record content lengths
     "longest", longest of all record content lengths
     "average", average of all record content lengths

     Make use of .mapToDouble and .orElse(Double.NaN).
     Hint: you will need to create three separate streams!
     */
    public Map<String, Double> statistics1(Collection<Record> records) {
        return TODO.implementMe();
    }

    /**
     There was an bunch of repetition in statistics1.
     Can you extract a function (Supplier)?
     */
    public Map<String, Double> statistics2(Collection<Record> records) {
        return TODO.implementMe();
    }

    /**
     The example solution to statistics2 still has some repetition...
     Start from that example solution, and extract a function (BiFunction) to remove all repetition
     */
    public Map<String, Double> statistics3(Collection<Record> records) {
        return TODO.implementMe();
    }
}
