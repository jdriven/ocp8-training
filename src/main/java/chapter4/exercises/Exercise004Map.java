package chapter4.exercises;

import java.util.Collection;
import java.util.List;
import java.util.Optional;


import chapter4.exercises.util.Person;
import chapter4.exercises.util.TODO;

public class Exercise004Map {
    /**
     * Using the map function, return a list of integers containing the negated versions
     * of the ints in the input list.
     * {1, -3, 4} -> {-1, 3, -4}
     */
    public List<Integer> negate(Collection<Integer> ints) {
        return TODO.implementMe();
    }

    /**
     * Return a list of, for each person of the input, their name.
     */
    public List<String> names(Collection<Person> persons) {
        return TODO.implementMe();
    }

    /**
     * Let's say for comparison purposes, we want the names in lowercase...
     * Use map twice, and use method reference.
     */
    public List<String> lowerCaseNames(Collection<Person> persons) {
        return TODO.implementMe();
    }

    /**
     * Given a list of names, return the length of the longest one.
     * By using names.stream().mapToInt instead of names.stream().map,
     * you get special methods you can use. Use your IDE to check them out!
     * In case of an empty collection, return 0 (you can do this without any if statements!).
     */
    public int lengthLongestName(Collection<String> names) {
        return TODO.implementMe();
    }

    /**
     * Same assignment, but now use .map and .reduce instead of .mapToInt
     */
    public int lengthLongestName2(Collection<String> names) {
        return TODO.implementMe();
    }

    /**
     * Return not the length of the shortest name, but the actual shortest name.
     * Use names.stream().min to solve this.
     */
    public static Optional<String> shortestName(Collection<String> names) {
        return TODO.implementMe();
    }
}
