package chapter4.exercises;

import java.util.Collection;
import java.util.Optional;
import java.util.function.BiFunction;


import chapter4.exercises.util.TODO;

public class Exercise005Reduce {
    /**
     Example code.
     */
    public static int sum(int a, int b) {
        return Integer.sum(a, b);
    }

    /**
     Example code.
     Reduce uses an 'identity' and an 'accumulator'.
     The identity is the starting point, and the accumulator is a function to combine the values.
     */
    public static int sum1(Collection<Integer> ints) {
        return ints.stream().reduce(0, Exercise005Reduce::sum);
    }

    /**
     Sum the ints using Integer.sum as accumulator.
     */
    public static int sum2(Collection<Integer> ints) {
        return TODO.implementMe();
    }

    /**
     Reduce's accumulator can be any BinaryOperator.
     Create a lambda function using the () -> lambda literal notation and use it as accumulator.
     */
    public static int sum3(Collection<Integer> ints) {
        return TODO.implementMe();
    }

    /**
     Instead of using a variable for the accumulator,
     don't use a variable and directly use a lambda literal.
     */
    public static int sum4(Collection<Integer> ints) {
        return TODO.implementMe();
    }

    /**
     Sometimes, you don't have a starting value for reduce.
     For example, when you want to find the max value using reduce and Math.max.
     In this case, reduce takes a single argument and returns an Optional,
     because the collection might be empty: then there is no result.
     */
    public static Optional<Integer> maxFromCollection(Collection<Integer> ints) {
        return TODO.implementMe();
    }

    /**
     The Java functions (Function, BiFunction, etc) can be called with func.apply(methods).
     Write your own simple version of reduce (for Collections directly, not streams), using a for-loop.
     */
    public static int myReduce(Collection<Integer> ints, int identity, BiFunction<Integer, Integer, Integer> func) {
        return TODO.implementMe();
    }

    /**
     Now make it generic.
     Note how in your own version, the start and return value can be of a different type than
     the elements in the collection.
     With some extra effort, this can be achieved in Java stream's reduce as well; see MiscExercises
     */
    public static <U, T> U myReduce(Iterable<T> collection, U identity, BiFunction<U, T, U> func) {
        return TODO.implementMe();
    }
}
