package chapter4.functional_interfaces;

import java.util.Collections;
import java.util.Random;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.BinaryOperator;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;

@SuppressWarnings("Convert2MethodRef")
public class CommonFunctionalInterfaces {

    public static void main(String[] args) {

        // Supplier
        System.out.println("Supplier");
        System.out.println("--------");
        Supplier<Integer> randomNumberSupplier = () -> RANDOM.nextInt(); // Evaluated lazily, so returns different value every time.
        System.out.println("randomNumberSupplier.get(): " + randomNumberSupplier.get());
        System.out.println("randomNumberSupplier.get(): " + randomNumberSupplier.get());
        System.out.println("randomNumberSupplier.get(): " + randomNumberSupplier.get());
        System.out.println();

        // Consumer
        System.out.println("Consumer");
        System.out.println("--------");
        Consumer<String> greeter = s -> System.out.println("Hello " + s);
        greeter.accept("World!");
        greeter.accept("OCP Class!");
        System.out.println();

        // BiConsumer
        System.out.println("BiConsumer");
        System.out.println("----------");
        BiConsumer<String, String> printFullName = (firstName, lastName) -> System.out.println(firstName + " " + lastName);
        printFullName.accept("Deniz", "Turan");
        printFullName.accept("Riccardo", "Lippolis");
        System.out.println();

        // Predicate
        System.out.println("Predicate");
        System.out.println("---------");
        Predicate<Integer> isEven = number -> number % 2 == 0;
        System.out.println("is 4 even? " + isEven.test(4));
        System.out.println("is 3 even? " + isEven.test(3));
        System.out.println();

        // BiPredicate
        System.out.println("BiPredicate");
        System.out.println("-----------");
        BiPredicate<Long, Integer> isDivisible = (num, div) -> num % div == 0;
        System.out.println("Is 10 divisible by 2? " + isDivisible.test(10L, 2));
        System.out.println("Is 5 divisible by 3? " + isDivisible.test(5L, 3));

        Predicate<Long> isEvenUsingIsDivisible = number -> isDivisible.test(number, 2);
        System.out.println("is 4 even (using isDivisible)? " + isEvenUsingIsDivisible.test(4L));
        System.out.println("is 3 even (using isDivisible)? " + isEvenUsingIsDivisible.test(3L));
        System.out.println();

        // Function
        System.out.println("Function");
        System.out.println("--------");
        Function<String, Integer> charCounter = text -> text.length();
        System.out.println("Length of 'hello': " + charCounter.apply("hello"));
        System.out.println("Length of 'something else': " + charCounter.apply("something else"));
        System.out.println();

        // BiFunction
        System.out.println("BiFunction");
        System.out.println("----------");
        BiFunction<String, Integer, String> repeater = (text, count) -> String.join(" ", Collections.nCopies(count, text));
        System.out.println("Repeat A 3x: " + repeater.apply("A", 3));
        System.out.println("Repeat B 4x: " + repeater.apply("B", 4));
        System.out.println("Repeat C 5x: " + repeater.apply("C", 5));
        System.out.println();

        // UnaryOperator
        System.out.println("UnaryOperator");
        System.out.println("-------------");
        UnaryOperator<Long> multiplier = n -> n * 2;
        System.out.println("Multiply 5 by 2: " + multiplier.apply(5L));
        System.out.println("Multiply 8 by 2 twice: " + multiplier.apply(multiplier.apply(8L)));
        System.out.println();

        // BinaryOperator
        System.out.println("BinaryOperator");
        System.out.println("--------------");
        BinaryOperator<Double> adder = (a, b) -> a + b;
        System.out.println("Add 1.23 to 4.56: " + adder.apply(1.23, 4.56));
        System.out.println();


        /// ----------------------------------------
        /// Some default methods in functional interfaces
        /// ----------------------------------------

        // Consumer (similar method exist in BiConsumer)
        System.out.println("Default methods in Consumer");
        System.out.println("----------------");
        Consumer<String> farewellGreeter = s -> System.out.println("Goodbye " + s);
        Consumer<String> combined = greeter.andThen(farewellGreeter);
        combined.accept("World!");
        System.out.println();

        // Predicate (similar methods exist in BiPredicate)
        System.out.println("Default methods in Predicate");
        Predicate<Integer> isSmall = n -> n <= 10;
        Predicate<Integer> isSmallEven = isEven.and(isSmall);
        Predicate<Integer> isSmallOdd = isSmall.and(isEven.negate());
        Predicate<Integer> isSmallOrEven = isSmall.or(isEven);
        System.out.println("is 10 small and even? " + isSmallEven.test(10));
        System.out.println("is 9 small and even? " + isSmallEven.test(9));
        System.out.println("is 64 small and even? " + isSmallEven.test(64));
        System.out.println("is 9 small and odd? " + isSmallOdd.test(9));
        System.out.println("is 9 small or even? " + isSmallOrEven.test(9));
        System.out.println("is 76 small or even? " + isSmallOrEven.test(76));
        System.out.println();

        // Function (similar method exists in BiFunction)
        System.out.println("Default methods in Function");
        Function<String, String> withoutSpaces = s -> s.replace(" ", "");
        Function<String, Integer> charCounterWithoutSpaces = withoutSpaces.andThen(charCounter);
        System.out.println("Length of 'hello' (without spaces): " + charCounterWithoutSpaces.apply("hello"));
        System.out.println("Length of 'something else' (without spaces): " + charCounterWithoutSpaces.apply("something else"));
        System.out.println();
    }

    private static final Random RANDOM = new Random();
}
