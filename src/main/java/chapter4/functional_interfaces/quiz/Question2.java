package chapter4.functional_interfaces.quiz;

public class Question2 {
}

interface AmountValidator {
    public boolean checkAmount(double value);
}

class Account {
    public void updateBalance(double bal) {
        boolean isOK = new AmountValidator() {
            public boolean checkAmount(double val) {
                return val >= 0.0;
            }
        }.checkAmount(bal);

        // other irrelevant code
    }
}

