package chapter4.functional_interfaces.quiz;

import java.util.HashMap;

public class Question1 {
    public static void main(String[] args) {

        HashMap<Integer, String> hm = new HashMap<>();
        hm.put(1, "a");
        hm.put(2, "b");
        hm.put(3, "c");

        // Answer 1:
        // hm.forEach((key, entry) -> System.out.println(entry));

        // Answer 2:
        // hm.forEach(System.out.println("%d %s"));

        // Answer 3:
        // hm.forEach((key, value) -> System.out.printf("%d %s ", key, value));

        // Answer 4:
        // hm.forEach(System.out::println);

        // Answer 5:
        // hm.forEach(entry -> System.out.println(entry));
    }
}
