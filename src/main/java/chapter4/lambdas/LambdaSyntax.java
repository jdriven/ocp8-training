package chapter4.lambdas;

import java.util.function.BinaryOperator;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;

@SuppressWarnings({"CodeBlock2Expr", "Convert2MethodRef"})
public class LambdaSyntax {

    public static void main(String[] args) {

        // Without parameters
        Supplier<String> lambdaWithoutParams = () -> "hello";
        Supplier<String> lambdaWithoutParamsWithBody = () -> {
            return "hello";
        };

        System.out.println("Lambda without params: " + lambdaWithoutParams.get());
        System.out.println("Lambda without params with body: " + lambdaWithoutParamsWithBody.get());


        // With one parameter
        UnaryOperator<String> lambdaWithParam = name -> "hello " + name;
        UnaryOperator<String> lambdaWithParamAndType = (String name) -> "hello " + name;
        UnaryOperator<String> lambdaWithParamAndBody = name -> {
            return "hello " + name;
        };
        UnaryOperator<String> lambdaWithParamAndTypeAndBody = (String name) -> {
            return "hello " + name;
        };

        System.out.println("Lambda with param: " + lambdaWithParam.apply("world"));
        System.out.println("Lambda with param and type: " + lambdaWithParamAndType.apply("world"));
        System.out.println("Lambda with param and body: " + lambdaWithParamAndBody.apply("world"));
        System.out.println("Lambda with param and type and body: " + lambdaWithParamAndTypeAndBody.apply("world"));


        // With multiple parameters
        BinaryOperator<Integer> lambdaWithParams = (a, b) -> a + b;
        BinaryOperator<Integer> lambdaWithParamsAndTypes = (Integer a, Integer b) -> a + b;
        BinaryOperator<Integer> lambdaWithParamsAndBody = (a, b) -> {
            return a + b;
        };
        BinaryOperator<Integer> lambdaWithParamsAndTypesAndBody = (Integer a, Integer b) -> {
            return a + b;
        };

        System.out.println("Lambda with params: " + lambdaWithParams.apply(1, 2));
        System.out.println("Lambda with params and type: " + lambdaWithParamsAndTypes.apply(1, 2));
        System.out.println("Lambda with params and body: " + lambdaWithParamsAndBody.apply(1, 2));
        System.out.println("Lambda with params and type and body: " + lambdaWithParamsAndTypesAndBody.apply(1, 2));
    }
}
