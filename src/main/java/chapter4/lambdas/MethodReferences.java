package chapter4.lambdas;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class MethodReferences {

    public static void main(String[] args) {
        new MethodReferences().testMethodReferences();
    }

    private void testMethodReferences() {
        List<Car> cars = new ArrayList<>();
        cars.add(new Car("Ferrari"));
        cars.add(new Car("BMW"));
        cars.add(new Car("Audi"));

        System.out.println("Cars: " + cars);

        List<Car> result1 = cars.stream()
                .map(Car::getBrand)             // Instance method reference on the Car object
                .map(Car::withBrand)            // Static method reference
                .collect(Collectors.toList());

        System.out.println("Result 1: " + result1);

        List<Car> result2 = cars.stream()
                .map(this::getCarBrand)         // Instance method reference on this object
                .map(Car::new)                  // Constructor method reference
                .collect(Collectors.toList());

        System.out.println("Result 2: " + result2);
    }

    private String getCarBrand(Car c) {
        return c.getBrand();
    }
}

class Car {

    private final String brand;

    Car(String brand) {
        this.brand = brand;
    }

    String getBrand() {
        return brand;
    }

    static Car withBrand(String brand) {
        return new Car(brand);
    }

    @Override
    public String toString() {
        return brand;
    }
}
