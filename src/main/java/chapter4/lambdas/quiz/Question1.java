package chapter4.lambdas.quiz;

import java.util.Arrays;
import java.util.List;

interface Carnivore {
    default int calories(List<String> food) {  return food.size()*100; }
    int eat(List<String> foods);
}
class Tiger implements Carnivore {
    public int eat(List<String> foods){
        System.out.println("Eating "+foods);
        return foods.size()*200;
    }
}
public class Question1 {
    public static int size(List<String> names) { return names.size() * 2;  }

    public static void process(List<String> names, Carnivore c) { c.eat(names); }

    public static void main(String[] args) {
        List<String> fnames = Arrays.asList("a", "b", "c");
        Tiger t = new Tiger();

        // INSERT CODE HERE

        // Answer 1:
        // process(fnames, t::eat);

        // Answer 2:
        // process(fnames, t::calories);

        // Answer 3:
        // process(fnames, TestClass::size);

        // Answer 4:
        // process(fnames, Carnivore::calories);

        // Answer 5:
        // process(fnames, Tiger::eat);
    }
}


