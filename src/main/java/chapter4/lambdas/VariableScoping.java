package chapter4.lambdas;

import java.util.function.Consumer;

@SuppressWarnings("UnusedAssignment")
public class VariableScoping {

    private static String staticMemberVariable = "Static member variable";

    private String instanceMemberVariable = "Instance member variable";

    public static void main(String[] args) {
        testScopingFromStaticContext();

        System.out.println();

        new VariableScoping().testScopingFromNonStaticContext();
    }

    private static void testScopingFromStaticContext() {
        String effectivelyFinalString = "Effectively final String";

        String nonFinalString = "String";
        nonFinalString = "Modified " + nonFinalString;

        Consumer<String> someLambda = (input) -> {
            System.out.println("Calling lambda in static context with input: " + input);
            // System.out.println("Non-final String: " + nonFinalString); // Does not compile, not effectively final!
            System.out.println("Effectively final String: " + effectivelyFinalString);
            // System.out.println("Instance member variable: " + instanceMemberVariable); // Does not compile, not reachable from static context!
            System.out.println("Static member variable: " + staticMemberVariable);
        };

        someLambda.accept("Some input");
    }

    private void testScopingFromNonStaticContext() {
        String effectivelyFinalString = "Effectively final String";

        String nonFinalString = "String";
        nonFinalString = "Modified " + nonFinalString;

        Consumer<String> someLambda = (input) -> {
            System.out.println("Calling lambda in non-static context with input: " + input);
            // System.out.println("Non-final String: " + nonFinalString); // Does not compile, not effectively final!
            System.out.println("Effectively final String: " + effectivelyFinalString);
            System.out.println("Instance member variable: " + instanceMemberVariable);
            System.out.println("Static member variable: " + staticMemberVariable);
        };

        someLambda.accept("Some input");
    }
}
