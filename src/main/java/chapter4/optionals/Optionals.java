package chapter4.optionals;

import java.util.Optional;
import java.util.Random;

@SuppressWarnings({"ConstantConditions", "OptionalIsPresent", "Convert2MethodRef"})
public class Optionals {
    public static void main(String[] args) {
        nonNullOptionals();
        possiblyNullOptionals();
        optionalOperations();
    }

    private static void nonNullOptionals() {
        Optional<String> nonNullValue = Optional.of("Some value");

        if (nonNullValue.isPresent()) {
            System.out.println("Non null value: " + nonNullValue.get());
        }

        nonNullValue.ifPresent(value -> System.out.println("Non null value from lambda: " + value));

        System.out.println("Non null value with exception if empty: " + nonNullValue.orElseThrow(IllegalArgumentException::new));
        System.out.println();
    }

    private static void possiblyNullOptionals() {
        // Optional<String> possiblyNullValue = Optional.of(null); // Throws NPE!
        Optional<String> possiblyNullValue = Optional.ofNullable(null);

        if (!possiblyNullValue.isPresent()) {
            // possiblyNullValue.get(); // Throws NoSuchElementException!
            System.out.println("PossiblyNullValue is not present");
        }

        possiblyNullValue.ifPresent(value -> System.out.println("This will not be printed, because the optional is empty"));

        System.out.println("Default value if empty: " + possiblyNullValue.orElse("default"));
        System.out.println("Default value if empty (lazy): " + possiblyNullValue.orElseGet(() -> generateDefault()));
        System.out.println("Default value if empty (lazy): " + possiblyNullValue.orElseGet(() -> generateDefault()));
        System.out.println();
    }

    private static void optionalOperations() {
        // Map
        Optional<Integer> optionalInteger = Optional.of(10);
        Optional<Integer> multiplied = optionalInteger.map(i -> i * 2);
        Optional<String> convertedToString = multiplied.map(String::valueOf);
        System.out.println("Mapped Optional: " + convertedToString.orElse("unknown"));

        String chainedMap = Optional.of(10)
                .map(i -> i * 2)
                .map(String::valueOf)
                .orElse("unknown");
        System.out.println("Mapped Optional (chained): " + chainedMap);

        // Filter
        Optional<Long> optionalLong = Optional.of(123L);
        Optional<Long> filtered = optionalLong.filter(l -> l % 2 == 0);
        System.out.println("Filtered optional: " + filtered.orElse(-1L));

        System.out.println();
    }

    private static String generateDefault() {
        return String.valueOf(RANDOM.nextInt(1000));
    }

    private static final Random RANDOM = new Random();
}
