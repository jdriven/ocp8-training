package chapter1.enums;

import java.util.Arrays;

public class EnumTest {
    public static void main(String[] args) {

        Direction direction = Direction.SOUTH;
        System.out.println("direction: " + direction);
        System.out.println("direction name: " + direction.name());
        System.out.println("direction ordinal: " + direction.ordinal());
        System.out.println("direction west: " + Direction.valueOf("WEST"));
        // System.out.println("direction west: " + Direction.valueOf("west")); // IllegalArgumentException! (case sensitive)
        System.out.println("direction values: " + Arrays.toString(Direction.values()));

        System.out.println();

        DirectionWithExtras directionWithExtras = DirectionWithExtras.EAST;
        System.out.println("direction with extras: " + directionWithExtras);
        System.out.println("direction with extras name: " + directionWithExtras.name());
        System.out.println("direction with extras ordinal: " + directionWithExtras.ordinal());
        System.out.println("direction with extras abbreviation: " + directionWithExtras.getAbbreviation());
        System.out.println("direction with extras opposite: " + directionWithExtras.getOpposite());
        System.out.println("direction with extras north: " + DirectionWithExtras.valueOf("NORTH"));
        System.out.println("direction with extras from abbreviation W: " + DirectionWithExtras.fromAbbreviation("W"));
        System.out.println("direction with extras values: " + Arrays.toString(DirectionWithExtras.values()));
    }
}

enum Direction {
    NORTH,
    SOUTH,
    EAST,
    WEST
}

enum DirectionWithExtras {
    NORTH("N") {
        @Override
        public DirectionWithExtras getOpposite() {
            return DirectionWithExtras.SOUTH;
        }
    },
    SOUTH("S") {
        @Override
        public DirectionWithExtras getOpposite() {
            return DirectionWithExtras.NORTH;
        }
    },
    EAST("E") {
        @Override
        public DirectionWithExtras getOpposite() {
            return DirectionWithExtras.WEST;
        }
    },
    WEST("W") {
        @Override
        public DirectionWithExtras getOpposite() {
            return DirectionWithExtras.EAST;
        }
    };

    private final String abbreviation;

    DirectionWithExtras(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public abstract DirectionWithExtras getOpposite();

    public static DirectionWithExtras fromAbbreviation(String abbr) {
        for (DirectionWithExtras value : values()) {
            if (value.getAbbreviation().equals(abbr)) {
                return value;
            }
        }
        throw new IllegalArgumentException("Unknown abbreviation!");
    }
}
