package chapter1.nested_classes;

public class StaticNestedClassTest {

    public static void main(String[] args) {
        OuterWithStatic.StaticInnerClass inner = new OuterWithStatic.StaticInnerClass();

        // OuterWithStatic outer = new OuterWithStatic();
        // StaticInnerClass inner = outer.new StaticInnerClass(); // Compile error!

        System.out.println("inner.getMemberWithSameNameOuter(): " + inner.getMemberWithSameNameOuter());
        System.out.println("inner.getMemberWithSameNameInner(): " + inner.getMemberWithSameNameInner());
    }
}

class OuterWithStatic {

    private int outerMember = 42; // Not static, therefore not accessible from static inner class

    private static String memberWithSameName = "bla outer";

    static class StaticInnerClass {

        private String memberWithSameName = "bla inner";

        String getMemberWithSameNameOuter() {
            return OuterWithStatic.memberWithSameName;
        }

        String getMemberWithSameNameInner() {
            return memberWithSameName; // or this.memberWithSameName
        }
    }
}
