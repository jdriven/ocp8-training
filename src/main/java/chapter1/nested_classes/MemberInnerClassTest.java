package chapter1.nested_classes;

import chapter1.nested_classes.Outer.MemberInnerClass;

public class MemberInnerClassTest {

    public static void main(String[] args) {
        // Outer.MemberInnerClass instance = new Outer.MemberInnerClass(); // Compile error!
        Outer outer = new Outer();
        MemberInnerClass inner = outer.new MemberInnerClass();
        System.out.println("inner.getOuterMemberValue(): " + inner.getOuterMemberValue());
        System.out.println("inner.getMemberWithSameNameOuter(): " + inner.getMemberWithSameNameOuter());
        System.out.println("inner.getMemberWithSameNameInner(): " + inner.getMemberWithSameNameInner());
    }
}

class Outer {

    private int outerMember = 42;

    private String memberWithSameName = "bla outer";

    class MemberInnerClass {

        private String memberWithSameName = "bla inner";

        int getOuterMemberValue() {
            return outerMember;
        }

        String getMemberWithSameNameOuter() {
            return Outer.this.memberWithSameName;
        }

        String getMemberWithSameNameInner() {
            return memberWithSameName; // or this.memberWithSameName
        }
    }
}
