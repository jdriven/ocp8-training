package chapter1.nested_classes;

import java.util.function.Function;

@SuppressWarnings("Convert2Lambda")
public class AnonymousInnerClassTest {

    public static void main(String[] args) {
        System.out.println("2 * 2 = " + methodThatUsesAnAnonymousInnerClassToCalculateSquare(2));
    }

    private static int methodThatUsesAnAnonymousInnerClassToCalculateSquare(int num) {
        Function<Integer, Integer> squareFunction = new Function<Integer, Integer>() {
            @Override
            public Integer apply(Integer i) {
                return i * i;
            }
        }; // Notice the semicolon!

        return squareFunction.apply(num);
    }
}
