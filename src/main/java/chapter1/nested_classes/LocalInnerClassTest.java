package chapter1.nested_classes;

public class LocalInnerClassTest {
    public static void main(String[] args) {
        // LocalInnerClass inner = new LocalInnerClass(2); // Compile error!

        System.out.println("2 * 2 = " + methodThatUsesALocalInnerClassToCalculateSquare(2));
    }

    private static int methodThatUsesALocalInnerClassToCalculateSquare(int num) {

        class LocalInnerClass {

            private int num;

            LocalInnerClass(int num) {
                this.num = num;
            }

            int getSquare() {
                return num * num;
            }
        }

        return new LocalInnerClass(num).getSquare();
    }
}
