package chapter1.override_overload;

import java.io.FileNotFoundException;
import java.io.IOException;

public class OverrideExamples {

    public String someMethod(String param) throws IOException {
        return "Parent method called with parameter: " + param;
    }

    public static String someStaticMethod(String param) {
        return "Parent static method called with parameter: " + param;
    }
}

class SubClass extends OverrideExamples {

    @Override
    public String someMethod(String param) throws FileNotFoundException {
        return "Child method called with parameter: " + param;
    }

    // No override annotation!
    public static String someStaticMethod(String param) {
        return "Child static method called with parameter: " + param;
    }
}

class Main {
    public static void main(String[] args) throws Exception {
        // Testing overrides
        OverrideExamples test1 = new OverrideExamples();
        System.out.println(test1.someMethod("OverrideExamples instance"));

        OverrideExamples test2 = new SubClass();
        System.out.println(test2.someMethod("SubClass instance"));





        // Testing hidden static methods
        OverrideExamples test3 = new OverrideExamples();
        System.out.println(test3.someStaticMethod("OverrideExamples instance"));

        OverrideExamples test4 = new SubClass();
        System.out.println(test4.someStaticMethod("SubClass instance, OverrideExamples variable"));

        SubClass test5 = new SubClass();
        System.out.println(test5.someStaticMethod("SubClass instance, SubClass variable"));
    }
}
