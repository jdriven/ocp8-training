package chapter1.override_overload;

import java.io.IOException;

@SuppressWarnings({"unused", "RedundantThrows"})
public class OverloadExamples {

    Number someOperation(Integer num, String text) throws IOException { return null; }

    // Different access level, different arguments, different exceptions
    protected Number someOperation(Integer num, String text, Object someObject) throws IllegalArgumentException { return null; }

    // Different return type, different arguments
    String someOperation(Integer num, String text, Double num2) { return null; }

    // Static methods can be overloaded
    static Long sum(Long num1, Long num2) { return null; }
    static Long sum(Long num1, Long num2, Long num3) { return null; }
}
