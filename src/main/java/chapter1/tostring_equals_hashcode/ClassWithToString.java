package chapter1.tostring_equals_hashcode;

public class ClassWithToString {

    private final String name;

    ClassWithToString(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return String.format("[Name: %s]", name);
    }

    // not an override
    public String tostring() {
        return "Blabla";
    }

    public static void main(String[] args) {
        ClassWithToString myObject = new ClassWithToString("test");

        System.out.println("my object: " + myObject);
        System.out.println("my object: " + myObject.toString());
        System.out.println("my object: " + myObject.tostring());
    }
}
