package chapter1.tostring_equals_hashcode;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@SuppressWarnings("Duplicates")
public class EqualsHashcodeTest {
    public static void main(String[] args) {
        testWithoutEqualsHashCode();
        testWithEqualsHashCode();
        testWithEqualsWithoutHashCode();
    }

    private static void testWithoutEqualsHashCode() {
        Set<ClassWithoutEqualsHashCode> set = new HashSet<>();
        ClassWithoutEqualsHashCode obj1 = new ClassWithoutEqualsHashCode(1);
        ClassWithoutEqualsHashCode obj2 = new ClassWithoutEqualsHashCode(2);
        set.add(obj1);
        set.add(obj2);

        System.out.println();
        System.out.println("Test without equals / hashCode:");
        System.out.println("Set contains obj1? " + set.contains(obj1));
        System.out.println("Set contains obj2? " + set.contains(obj2));

        System.out.println("Set contains object similar to obj1? " + set.contains(new ClassWithoutEqualsHashCode(1)));
        System.out.println("Set contains object similar to obj2? " + set.contains(new ClassWithoutEqualsHashCode(2)));
    }

    private static void testWithEqualsHashCode() {
        Set<ClassWithEqualsHashCode> set = new HashSet<>();
        ClassWithEqualsHashCode obj1 = new ClassWithEqualsHashCode(1);
        ClassWithEqualsHashCode obj2 = new ClassWithEqualsHashCode(2);
        set.add(obj1);
        set.add(obj2);

        System.out.println();
        System.out.println("Test with equals / hashCode:");
        System.out.println("Set contains obj1? " + set.contains(obj1));
        System.out.println("Set contains obj2? " + set.contains(obj2));

        System.out.println("Set contains object similar to obj1? " + set.contains(new ClassWithEqualsHashCode(1)));
        System.out.println("Set contains object similar to obj2? " + set.contains(new ClassWithEqualsHashCode(2)));
    }

    private static void testWithEqualsWithoutHashCode() {
        Set<ClassWithEqualsWithoutHashCode> set = new HashSet<>();
        ClassWithEqualsWithoutHashCode obj1 = new ClassWithEqualsWithoutHashCode(1);
        ClassWithEqualsWithoutHashCode obj2 = new ClassWithEqualsWithoutHashCode(2);
        set.add(obj1);
        set.add(obj2);

        System.out.println();
        System.out.println("Test with equals, without hashCode:");
        System.out.println("Set contains obj1? " + set.contains(obj1));
        System.out.println("Set contains obj2? " + set.contains(obj2));

        System.out.println("Set contains object similar to obj1? " + set.contains(new ClassWithEqualsWithoutHashCode(1)));
        System.out.println("Set contains object similar to obj2? " + set.contains(new ClassWithEqualsWithoutHashCode(2)));

        System.out.println("obj1 equals new ClassWithEqualsWithoutHashCode(1)? " + (obj1.equals(new ClassWithEqualsWithoutHashCode(1))));
        System.out.println("obj2 equals new ClassWithEqualsWithoutHashCode(2)? " + (obj2.equals(new ClassWithEqualsWithoutHashCode(2))));
    }
}

class ClassWithoutEqualsHashCode {

    private final long id;

    ClassWithoutEqualsHashCode(long id) {
        this.id = id;
    }

    long getId() {
        return id;
    }
}

class ClassWithEqualsHashCode {

    private final long id;

    ClassWithEqualsHashCode(long id) {
        this.id = id;
    }

    long getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ClassWithEqualsHashCode)) return false;
        return getId() == ((ClassWithEqualsHashCode) o).getId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}

class ClassWithEqualsWithoutHashCode {

    private final long id;

    ClassWithEqualsWithoutHashCode(long id) {
        this.id = id;
    }

    long getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ClassWithEqualsWithoutHashCode)) return false;
        return getId() == ((ClassWithEqualsWithoutHashCode) o).getId();
    }
}
