package chapter1.accessmodifiers.subpackage;

import chapter1.accessmodifiers.ParentClass;

public class ChildClassInSubPackage extends ParentClass {

    public void doSomething() {
        publicMethod();
        // defaultMethod();
        protectedMethod();
        // privateMethod();
    }
}
