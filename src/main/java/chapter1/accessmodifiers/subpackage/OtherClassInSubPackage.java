package chapter1.accessmodifiers.subpackage;

import chapter1.accessmodifiers.ChildClass;

public class OtherClassInSubPackage {

    public void doSomethingWithChildClass() {
        ChildClass childClass = new ChildClass();
        childClass.doSomething();
        childClass.publicMethod();
        // childClass.defaultMethod();
        // childClass.protectedMethod();
        // childClass.privateMethod();
    }

    public void doSomethingWithChildClassInSubPackage() {
        ChildClassInSubPackage childClassInSubPackage = new ChildClassInSubPackage();
        childClassInSubPackage.doSomething();
        childClassInSubPackage.publicMethod();
        // childClassInSubPackage.defaultMethod();
        // childClassInSubPackage.protectedMethod();
        // childClassInSubPackage.privateMethod();
    }
}
