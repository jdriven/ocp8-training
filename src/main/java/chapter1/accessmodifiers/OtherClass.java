package chapter1.accessmodifiers;

import chapter1.accessmodifiers.subpackage.ChildClassInSubPackage;

public class OtherClass {

    public void doSomethingWithChildClass() {
        ChildClass childClass = new ChildClass();
        childClass.doSomething();
        childClass.publicMethod();
        childClass.defaultMethod();
        childClass.protectedMethod();
        // childClass.privateMethod();
    }

    public void doSomethingWithChildClassInSubPackage() {
        ChildClassInSubPackage childClassInSubPackage = new ChildClassInSubPackage();
        childClassInSubPackage.doSomething();
        childClassInSubPackage.publicMethod();
        // childClassInSubPackage.defaultMethod();
        childClassInSubPackage.protectedMethod();
        // childClassInSubPackage.privateMethod();
    }
}
