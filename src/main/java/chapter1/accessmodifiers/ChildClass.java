package chapter1.accessmodifiers;

public class ChildClass extends ParentClass {

    public void doSomething() {
        publicMethod();
        defaultMethod();
        protectedMethod();
        // privateMethod();
    }
}
