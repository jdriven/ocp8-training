package chapter1.accessmodifiers;

public abstract class ParentClass {

    public void publicMethod() { System.out.println("ParentClass.publicMethod()"); }

    void defaultMethod() { System.out.println("ParentClass.defaultMethod()"); }

    protected void protectedMethod() { System.out.println("ParentClass.protectedMethod()"); }

    private void privateMethod() { System.out.println("ParentClass.privateMethod()"); }
}
