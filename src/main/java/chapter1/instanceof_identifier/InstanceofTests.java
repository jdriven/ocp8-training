package chapter1.instanceof_identifier;

@SuppressWarnings({"ConstantConditions", "WrapperTypeMayBePrimitive"})
public class InstanceofTests {

    public static void main(String[] args) {
        Long number = 42L;
        System.out.println("number instanceof Long? " + (number instanceof Long));
        System.out.println("number instanceof Number? " + (number instanceof Number));
        System.out.println("number instanceof Object? " + (number instanceof Object));
        // System.out.println("number instanceof String? " + (number instanceof String)); // Won't compile

        InterfaceA instanceOfA = new InterfaceA() {};
        System.out.println("instanceOfA instanceof InterfaceA? " + (instanceOfA instanceof InterfaceA));
        System.out.println("instanceOfA instanceof InterfaceB? " + (instanceOfA instanceof InterfaceB)); // Compiles!

        ImplementationX implementationX = new ImplementationX();
        System.out.println("implementationX instanceof ImplementationX? " + (implementationX instanceof ImplementationX));
        System.out.println("implementationX instanceof InterfaceA? " + (implementationX instanceof InterfaceA));
        System.out.println("implementationX instanceof InterfaceB? " + (implementationX instanceof InterfaceB));

        System.out.println("null instanceof String? " + (null instanceof String));
        String s = null;
        System.out.println("s instanceof String? " + (s instanceof String));
    }

    private interface InterfaceA {}
    private interface InterfaceB {}
    private static class ImplementationX implements InterfaceA, InterfaceB {}
}
