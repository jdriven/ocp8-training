package chapter1.quiz;

public class Question3 {
    public static void main(String[] args) {

        Resource resource1 = new Resource();
        resource1.setData("test");
        Resource resource2 = new Resource();
        resource2.setData("test");
        System.out.println("Answer 1 & 2: " + (resource1.equals(resource2)));

        System.out.println("Answer 3: " + (resource1.equals((Object) resource2)));

        System.out.println("Answer 6 & 7: " + new Resource().equals(new Object()));
    }
}

class Resource {
    private String data = "DATA";
    String getData() {
        return data;
    }

    void setData(String data) {
        this.data = data == null ? "" : data;
    }

    boolean equals(Resource r) {
        return (r != null && r.getData().equals(this.getData()));
    }
}
