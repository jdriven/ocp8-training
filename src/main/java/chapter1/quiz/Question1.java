package chapter1.quiz;

import java.util.HashMap;
import java.util.Map;

public class Question1 {
}

class Book {
    private String title, isbn;
    public boolean equals(Object o) {
        return (o instanceof Book && ((Book)o).isbn.equals(this.isbn));
    }

    String getTitle() {
        return title;
    }

    void setTitle(String title) {
        this.title = title;
    }

    String getIsbn() {
        return isbn;
    }

    void setIsbn(String isbn) {
        this.isbn = isbn;
    }
}

class BookStore {
    Map<Book, Integer> map = new HashMap<>();
    int getNumberOfCopies(Book b){
        return map.get(b);
    }
    public void addBook(Book b, int numberofcopies){
        map.put(b, numberofcopies);
    }
    // ... other useful methods.
}

class TestClass {
    static BookStore bs = new BookStore();

    public static void main(String[] args) {
        Book b = new Book(); b.setIsbn("111");
        bs.addBook(b, 10);
        System.out.println(bs.getNumberOfCopies(b));

        b = new Book(); b.setIsbn("111");
        System.out.println(bs.getNumberOfCopies(b));
    }
}


