package chapter1.quiz;

import chapter1.quiz.p1.A;
import chapter1.quiz.p2.B;

public class Question4 {
    public static void main(String[] args) {
        A a = new B();
        B b = new B();
        b.process(a);
        System.out.println(a.getI());
    }
}
