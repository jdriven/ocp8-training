package chapter10;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DatabaseExceptions {

    public static void main(String[] args) throws SQLException {

        DatabaseUtil.setupDatabase();

        try (Connection conn = DriverManager.getConnection(DatabaseUtil.JDBC_URL);
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery("select not_a_column from animal")) {

            while (rs.next()) {
                System.out.println(rs.getString(1));
            }
        } catch (SQLException e) {
            System.out.println("Message:   " + e.getMessage());
            System.out.println("SQLState:  " + e.getSQLState());
            System.out.println("ErrorCode: " + e.getErrorCode());
        }
    }
}
