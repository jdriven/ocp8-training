package chapter10;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DataTypes {
    public static void main(String[] args) throws SQLException {

        DatabaseUtil.setupDatabase();

        try (Connection conn = DriverManager.getConnection(DatabaseUtil.JDBC_URL);
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery("select id, species_id, name, date_born from animal")) {

            while (rs.next()) {
                System.out.println("ID: " + rs.getInt("id"));
                System.out.println("Species ID: " + rs.getInt("species_id"));
                System.out.println("Name: " + rs.getString("name"));
                System.out.println("Date born (SQL date): " + rs.getDate("date_born"));
                System.out.println("Date born (Local date): " + rs.getDate("date_born").toLocalDate());
                System.out.println("Time born (SQL time): " + rs.getTime("date_born"));
                System.out.println("Time born (Local time): " + rs.getTime("date_born").toLocalTime());
                System.out.println("Timestamp born (SQL timestamp): " + rs.getTimestamp("date_born"));
                System.out.println("Timestamp born (Local date time): " + rs.getTimestamp("date_born").toLocalDateTime());
                System.out.println("Default class when getting date_born (as Object): " + rs.getObject("date_born").getClass());
                System.out.println();
            }
        }
    }
}
