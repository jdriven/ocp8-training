package chapter10;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class MyFirstDatabaseConnection {
    public static void main(String[] args) throws SQLException {

        DatabaseUtil.setupDatabase();

        try (Connection conn = DriverManager.getConnection(DatabaseUtil.JDBC_URL);
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery("select name from animal")) {

            System.out.println("Query results:");
            while (rs.next()) {
                System.out.println(rs.getString(1)); // Index starts at 1!
                // rs.previous(); // throws SQLException: The result set is not scrollable!
            }
        }
    }
}
