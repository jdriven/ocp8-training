package chapter10.quiz;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DBQuestion {
    public void question1() throws SQLException {
        // Consider the following code:
        Connection connection = DriverManager.getConnection("jdbc:derby://localhost:1527/sample");
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery("select * from CUSTOMER" +
            " where EMAILID = 'bob@gmail.com'"); //LINE 10
        while(rs.next()){
            System.out.println(rs.getString("emailid")); //LINE 12
        }
        connection.close();

        /* Assuming that the query returns exactly 1 row, what will be printed when this code is run?
           (Assume that items not specified such as import statements, DB url, and try/catch block are all valid)


           1. It will throw an exception at //LINE 12

           2. Compilation will fail due to //LINE 12

           3. It will print bob@gmail.com

           4. It will print bob@gmail.com and then throw an exception

         */

    }

    public void question2() throws SQLException {
        // Consider the following code:

        Connection c = DriverManager.getConnection("jdbc:derby://localhost:1527/sample");
        Statement stmt = c.createStatement();
        ResultSet rs = stmt.executeQuery("select * from STUDENT");
        stmt.close();
        while(rs.next()){
            System.out.println(rs.getInt(1));
        }
        c.close();

        /* Assuming that STUDENT table has 2 records containing values 1 and 2 for the first column,
           what will this code print?
           (Assume that items not specified such as import statements, DB url, and try/catch block are all valid)


         1. 1
            2


         2. 2


         3. Exception at run time


         4. Compilation failure

        */
    }
}
