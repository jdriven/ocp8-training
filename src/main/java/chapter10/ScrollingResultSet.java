package chapter10;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ScrollingResultSet {
    public static void main(String[] args) throws SQLException {

        DatabaseUtil.setupDatabase();

        try (Connection conn = DriverManager.getConnection(DatabaseUtil.JDBC_URL);
             Statement stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
             ResultSet rs = stmt.executeQuery("select name from species")) {

            System.out.println("Scrollable query results:");

            rs.next();
            System.out.println("rs.next():                     " + rs.getString(1));

            rs.next();
            System.out.println("rs.next():                     " + rs.getString(1));

            rs.previous();
            System.out.println("rs.previous():                 " + rs.getString(1));

            rs.last();
            System.out.println("rs.last():                     " + rs.getString(1));

            rs.first();
            System.out.println("rs.first():                    " + rs.getString(1));

            rs.afterLast();
            rs.previous();
            System.out.println("rs.afterLast(); rs.previous(): " + rs.getString(1));

            rs.beforeFirst();
            rs.next();
            System.out.println("rs.beforeFirst(); rs.next():   " + rs.getString(1));

            rs.absolute(1);
            System.out.println("rs.absolute(1):                " + rs.getString(1));

            rs.absolute(2);
            System.out.println("rs.absolute(2):                " + rs.getString(1));

            rs.absolute(-1);
            System.out.println("rs.absolute(-1):               " + rs.getString(1));

            rs.absolute(-2);
            System.out.println("rs.absolute(-2):               " + rs.getString(1));

            rs.relative(1);
            System.out.println("rs.relative(1):                " + rs.getString(1));

            rs.relative(-1);
            System.out.println("rs.relative(-1):               " + rs.getString(1));
        }
    }
}
