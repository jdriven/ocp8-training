package chapter8;

import java.io.Console;
import java.util.Arrays;
import java.util.Random;

/**
 * NOTE: if you run this class directly from your IDE, it will most likely not work because no Console is available.
 *
 * As an alternative, open a shell, navigate to src/main/java, and execute the following commands:
 *
 * > javac ./chapter8/ConsoleExample.java
 * > java chapter8.ConsoleExample
 *
 */
public class ConsoleExample {
    public static void main(String[] args) {
        Console console = System.console();
        if (console == null) {
            throw new IllegalStateException("No console is present!");
        }

        char[] password = console.readPassword("Enter password to continue: ");

        if (!Arrays.equals(password, "admin".toCharArray())) {
            console.printf("The password was incorrect!");
            System.exit(1);
        }
        Arrays.fill(password, 'x');

        console.writer().println("Guess a number between 1 and 100!");
        int num = new Random().nextInt(100) + 1;

        int nrOfGuesses = 0;
        while (true) {
            String guessStr = console.readLine("Enter your guess: ");
            int guess;
            try {
                guess = Integer.parseInt(guessStr);
            } catch (NumberFormatException e) {
                console.printf("Not a valid number: %s%n", guessStr);
                continue;
            }
            nrOfGuesses++;

            if (guess == num) {
                console.writer().println("You guessed it! It took you " + nrOfGuesses + " guesses!");
                break;
            } else if (guess < num) {
                console.writer().printf("Your guess (%d) was too low!%n", guess);
            } else {
                console.writer().format("Your guess (%d) was too high!%n", guess);
            }
        }
    }
}
