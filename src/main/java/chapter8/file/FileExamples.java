package chapter8.file;

import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.util.Arrays;


import utils.FileUtils;

@SuppressWarnings("DuplicatedCode")
public class FileExamples {
    public static void main(String[] args) throws IOException {

        File file = new File("/tmp/bladiebla.txt");
        System.out.println("File name: " + file.getName());
        System.out.println("Exists: " + file.exists());
        System.out.println("Absolute path: " + file.getAbsolutePath());
        System.out.println("Is directory: " + file.isDirectory());
        System.out.println("Is file: " + file.isFile());
        System.out.println();

        File tempFile = FileUtils.createTempFile("Some text");
        System.out.println("File name: " + tempFile.getName());
        System.out.println("Exists: " + tempFile.exists());
        System.out.println("Absolute path: " + tempFile.getAbsolutePath());
        System.out.println("Is directory: " + tempFile.isDirectory());
        System.out.println("Is file: " + tempFile.isFile());
        System.out.println("Length: " + tempFile.length());
        System.out.println("Last modified: " + Instant.ofEpochMilli(tempFile.lastModified()));

        String newName = String.format("%s%srenamed-%s", tempFile.getParent(), System.getProperty("file.separator"), tempFile.getName());
        System.out.println("Renaming to " + newName);
        File newFile = new File(newName);
        System.out.println("Rename succeeded? " + tempFile.renameTo(newFile));

        System.out.println("New file exists? " + newFile.exists());
        System.out.println("Delete succeeded? " + newFile.delete());
        System.out.println("New file exists? " + newFile.exists());

        File parentDir = new File(newFile.getParent());

        File subdirectories = new File(parentDir, "subdir1" + System.getProperty("file.separator") + "subdir2");
        System.out.println("Creating subdirectories: " + subdirectories.getPath());
        System.out.println("Subdirectories created? " + subdirectories.mkdirs());
        System.out.println("Is directory: " + subdirectories.isDirectory());

        System.out.println("Files in parent dir: " + Arrays.toString(parentDir.listFiles()));
    }
}
