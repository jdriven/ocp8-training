package chapter8.streams;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;


import utils.FileUtils;

public class ObjectStreams {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        File tmpFile = FileUtils.createTempFile();

        objectOutputStream(tmpFile);
        System.out.println();
        objectInputStream(tmpFile);
    }

    private static void objectOutputStream(File tmpFile) throws IOException {
        System.out.println("ObjectOutputStream:");

        List<DomainObject> objects = Arrays.asList(
                new DomainObject(1, "Object 1", "T1"),
                new DomainObject(2, "Object 2", "T2"),
                new DomainObject(3, "Object 3", "T3"),
                new DomainObject(4, "Object 4", "T4"),
                new DomainObject(5, "Object 5", "T5")
        );

        List<DomainObjectWithCustomSerialization> objectsWithCustomSerialization = Arrays.asList(
                new DomainObjectWithCustomSerialization(1, "Custom Object 1", "C1"),
                new DomainObjectWithCustomSerialization(2, "Custom Object 2", "C2"),
                new DomainObjectWithCustomSerialization(3, "Custom Object 3", "C3")
        );

        try (FileOutputStream fileOut = new FileOutputStream(tmpFile);
             BufferedOutputStream bufferedOut = new BufferedOutputStream(fileOut);
             ObjectOutputStream objectOut = new ObjectOutputStream(bufferedOut)) {

            System.out.println("Writing objects");
            for (DomainObject object : objects) {
                System.out.println("Writing: " + object);
                objectOut.writeObject(object);
            }
            System.out.println("Done writing objects");

            System.out.println("Writing custom objects");
            for (DomainObjectWithCustomSerialization customObject : objectsWithCustomSerialization) {
                System.out.println("Writing: " + customObject);
                objectOut.writeObject(customObject);
            }
            System.out.println("Done writing custom objects");
        }
    }

    private static void objectInputStream(File tmpFile) throws IOException, ClassNotFoundException {
        System.out.println("ObjectInputStream:");

        try (FileInputStream fileIn = new FileInputStream(tmpFile);
             BufferedInputStream bufferedIn = new BufferedInputStream(fileIn);
             ObjectInputStream objectIn = new ObjectInputStream(bufferedIn)) {

            System.out.println("Reading objects");
            while (true) {
                try {
                    Object object = objectIn.readObject();
                    System.out.println("Read: " + object);
                } catch (EOFException e) {
                    break;
                }
            }
            System.out.println("Done reading");
        }
    }
}


class DomainObject implements Serializable {

    private static final long serialVersionUID = 1L;

    private final long id;
    private final String description;
    private transient String transientText = "bla";

    DomainObject(long id, String description, String transientText) {
        this.id = id;
        this.description = description;
        this.transientText = transientText;
    }

    long getId() {
        return id;
    }

    String getDescription() {
        return description;
    }

    String getTransientText() {
        return transientText;
    }

    @Override
    public String toString() {
        return String.format("DomainObject[%d - \"%s\" - \"%s\"]", id, description, transientText);
    }
}


class DomainObjectWithCustomSerialization implements Serializable {
    private static final long serialVersionUID = 1L;

    private long id;
    private transient String description;
    private transient String transientText = "bla";

    DomainObjectWithCustomSerialization(long id, String description, String transientText) {
        this.id = id;
        this.description = description;
        this.transientText = transientText;
    }

    long getId() {
        return id;
    }

    String getDescription() {
        return description;
    }

    String getTransientText() {
        return transientText;
    }

    @Override
    public String toString() {
        return String.format("DomainObjectWithCustomSerialization[%d - \"%s\" - \"%s\"]", id, description, transientText);
    }

    private void readObject(ObjectInputStream is) throws Exception {
        is.defaultReadObject();
        description = (String) is.readObject();
        transientText = (String) is.readObject();
    }

    private void writeObject(ObjectOutputStream os) throws Exception {
        os.defaultWriteObject();
        os.writeObject(description);
        os.writeObject(transientText);
    }
}
