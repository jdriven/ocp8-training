package chapter8.streams;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.LocalTime;


import utils.FileUtils;

public class PrintStreams {
    public static void main(String[] args) throws IOException {
        printStream();
        System.out.println();
        printWriter();
    }

    private static void printStream() throws IOException {
        System.out.println("PrintStream:");
        File tmpFile = FileUtils.createTempFile();

        System.out.println("Writing to file");
        try (PrintStream printStream = new PrintStream(tmpFile)) {
            printStream.println(123);
            printStream.println("String");
            printStream.println(new DomainObject(111, "desc", "text"));
            printStream.printf("Today is %s%n", LocalDate.now().getDayOfWeek());
            printStream.format("The time is: %s", LocalTime.now());
        }
        System.out.println("Done writing");
        System.out.println("File contents:");
        System.out.println(FileUtils.getContentAsString(tmpFile));
    }

    private static void printWriter() throws IOException {
        System.out.println("PrintWriter:");
        File tmpFile = FileUtils.createTempFile();

        System.out.println("Writing to file");
        try (PrintWriter printWriter = new PrintWriter(tmpFile)) {
            printWriter.println(123);
            printWriter.println("String");
            printWriter.println(new DomainObject(111, "desc", "text"));
            printWriter.printf("Today is %s%n", LocalDate.now().getDayOfWeek());
            printWriter.format("The time is: %s", LocalTime.now());
        }
        System.out.println("Done writing");
        System.out.println("File contents:");
        System.out.println(FileUtils.getContentAsString(tmpFile));
    }
}
