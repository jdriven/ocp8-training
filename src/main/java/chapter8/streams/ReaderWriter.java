package chapter8.streams;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;


import utils.FileUtils;

public class ReaderWriter {

    public static void main(String[] args) throws IOException {
        File tmpFile = FileUtils.createTempFile();

        fileWriter(tmpFile);
        System.out.println();
        fileReader(tmpFile);
    }

    private static void fileWriter(File tmpFile) throws IOException {
        System.out.println("FileWriter:");
        List<String> lines = Arrays.asList(
                "line 1",
                "line 2",
                "line 3",
                "line 4",
                "line 5"
        );

        System.out.println("Writing lines");
        try (FileWriter writer = new FileWriter(tmpFile);
             BufferedWriter bufferedWriter = new BufferedWriter(writer)) {

            for (String line : lines) {
                System.out.println("Write line: " + line);
                bufferedWriter.write(line);
                bufferedWriter.newLine();
            }
        }
        System.out.println("Done writing");
    }

    private static void fileReader(File tmpFile) throws IOException {
        System.out.println("FileReader:");

        System.out.println("Reading lines");
        try (FileReader reader = new FileReader(tmpFile);
             BufferedReader bufferedReader = new BufferedReader(reader)) {

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                System.out.println("Read line: " + line);
            }
        }
        System.out.println("Done reading");
    }
}
