package chapter8.streams;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;


import utils.FileUtils;
import utils.LoggingInputStream;
import utils.LoggingOutputStream;

public class BufferedStreams {
    public static void main(String[] args) throws IOException {
        File tmpFile = FileUtils.createTempFile();

        bufferedOutputStream(tmpFile);
        System.out.println();
        bufferedInputStream(tmpFile);
    }

    private static void bufferedOutputStream(File tmpFile) throws IOException {
        byte[] content = new byte[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
        System.out.println("BufferedOutputStream:");

        int bufferSize = 4;
        try (OutputStream fileOut = LoggingOutputStream.wrap(new FileOutputStream(tmpFile));
             OutputStream bufferedOut = LoggingOutputStream.wrap(new BufferedOutputStream(fileOut, bufferSize))) {

            System.out.println("Start writing");
            for (byte b : content) {
                bufferedOut.write(b);

            }
            System.out.println("Done writing");
        }
    }

    private static void bufferedInputStream(File tmpFile) throws IOException {
        System.out.println("BufferedInputStream:");

        int bufferSize = 4;
        try (InputStream fileIn = LoggingInputStream.wrap(new FileInputStream(tmpFile));
             InputStream bufferedIn = LoggingInputStream.wrap(new BufferedInputStream(fileIn, bufferSize))) {

            System.out.println("Start reading");
            int b;
            while ((b = bufferedIn.read()) != -1) {
                System.out.println("Read: " + b);
            }

            System.out.println("Done reading");
        }
    }
}
