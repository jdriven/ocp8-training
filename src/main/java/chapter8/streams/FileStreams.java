package chapter8.streams;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;


import utils.FileUtils;

public class FileStreams {

    public static void main(String[] args) throws IOException {
        fileInputStream();
        System.out.println();
        fileOutputStream();
    }

    private static void fileInputStream() throws IOException {
        System.out.println("FileInputStream:");
        File tmpInFile = FileUtils.createTempFile("Some content");
        try (FileInputStream in = new FileInputStream(tmpInFile)) {
            int b;
            while ((b = in.read()) != -1) {
                System.out.print(b + " ");
            }
        }
        System.out.println();
    }

    private static void fileOutputStream() throws IOException {
        System.out.println("FileOutputStream:");
        File tmpOutFile = FileUtils.createTempFile();
        byte[] content = new byte[] { 83, 111, 109, 101, 32, 99, 111, 110, 116, 101, 110, 116};

        try (FileOutputStream out = new FileOutputStream(tmpOutFile)) {
            out.write(content);
        }
        System.out.println(FileUtils.getContentAsString(tmpOutFile));
    }
}
