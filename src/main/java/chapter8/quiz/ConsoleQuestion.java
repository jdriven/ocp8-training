package chapter8.quiz;

import java.io.Console;

public class ConsoleQuestion {

    // Consider the following code:

    public static void main(String[] args) {
        Console c = System.console();
//        String id = c.readLine("%s", "Enter UserId:"); //1
//        System.out.println("userid is " + id); //2
//        String pwd = c.readPassword("%s", "Enter Password :"); //3
//        System.out.println("password is " + pwd); //4

        /*
           Assuming that c is a valid reference to java.io.Console and that a user
           types jack as userid and jj123 as password, what will be the output on the console?


            Enter UserId:jack
        1.  userid is jack
            Enter Password :
            password is jj123


            Enter UserId:jack
        2.  userid is jack
            Enter Password :*****
            password is jj123


            Enter UserId:jack
            userid is jack
        3.  Enter Password :
            password is *****

            Enter UserId:jack
        4.  userid is jack
            Enter Password : password is jj123

        5.  It will not compile.





         */
    }




}
