package chapter8.quiz;

public class BufferedReaderQuestion {

    public void processLines(String fullFilePath) throws Exception {
        // declare and initialize "handle" here
//        String str = null;
//        while ((str = handle.readLine()) != null) {
//            System.out.println("Processing line : " + str);
//        }
//        handle.close();
    }

    /*
       Which of the given options will declare and initialize handle appropriately? Choose 2

        1. Reader handle = new FileReader(fullFilePath);

        2. BufferedReader handle = new BufferedReader(fullFilePath);

        3. BufferedReader handle = new BufferedReader(new File(fullFilePath));

        4. BufferedReader handle = new BufferedReader(new FileReader(fullFilePath));

        5. BufferedReader handle = new BufferedReader(new FileReader(new File(fullFilePath)));

     */




}
