package chapter8.quiz;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class SerializableQuestion {

    class Bond {
        String ticker;
        double coupon;
        java.util.Date maturity;
    }

    class Portfolio implements Serializable {
        String accountName;
        Bond[] bonds;
    }


    /*
     * Which of the following approaches can be taken independent of each other so that a Portfolio object can be
     * serialized while perserving the state of the Bond objects contained in Portfolio? Select 2 options
     *
     * 1. It can be serialized as it is without any modification
     * 2. Just have Bond class implement Serializable
     * 3. Just make 'bonds' field in Portfolio transient
     * 4. Change the type of bonds from Bond[] to ArrayList<Bond> bonds;
     * 5. Make bonds array transient, and implement readObject(ObjectInputStream os) and
     *    writeObject(ObjectOutputStream methods to read and write the state of Bond objects explicitly
     */
}

class Answer5Explained {
    class Bond {
        String ticker = "bac";
        double coupon = 8.3;
        java.util.Date maturity = new java.util.Date();
    }

    class Portfolio implements Serializable {
        String accountName;
        transient Bond[] bonds = new Bond[] {}; // must be transient because Bond class does not implement Serializable

        private void writeObject(ObjectOutputStream os) throws Exception {
            os.defaultWriteObject();
            os.writeInt(bonds.length);
            //write the state of bond objects
            for (final Bond bond : bonds) {
                os.writeObject(bond.ticker);
                os.writeDouble(bond.coupon);
                os.writeObject(bond.maturity);
            }
        }

        private void readObject(ObjectInputStream os) throws Exception {
            os.defaultReadObject();
            int n = os.readInt();
            this.bonds = new Bond[n];
            //read the state of bond objects.
            for (int i = 0; i < bonds.length; i++) {
                bonds[i] = new Bond();
                bonds[i].ticker = (String) os.readObject();
                bonds[i].coupon = os.readDouble();
                bonds[i].maturity = (java.util.Date) os.readObject();
            }

        }
    }

}
