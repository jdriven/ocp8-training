package chapter7.atomic;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

public class AtomicBooleanExample {

    private static final int NR_OF_THREADS = 10;

    public static void main(String[] args) {
        ExecutorService service = null;
        try {
            service = Executors.newFixedThreadPool(NR_OF_THREADS);

            AtomicBoolean atomicBoolean = new AtomicBoolean(false);

            for (int i = 1; i <= NR_OF_THREADS; i++) {
                int threadNr = i;
                service.submit(() -> {
                    if (atomicBoolean.compareAndSet(false, true)) {
                        System.out.println("Thread " + threadNr + " has won!");
                    }
                });
            }
        } finally {
            if (service != null) {
                service.shutdown();
            }
        }
    }
}
