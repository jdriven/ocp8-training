package chapter7.atomic;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class AtomicIntegerExample {

    private static final int NR_OF_THREADS = 100;

    private static int unsafeCounter = 0;
    private static AtomicInteger safeCounter = new AtomicInteger(0);

    public static void main(String[] args) throws InterruptedException {
        ExecutorService service = null;
        try {
            service = Executors.newFixedThreadPool(NR_OF_THREADS);

            for (int i = 1; i <= NR_OF_THREADS; i++) {
                service.submit(() -> {
                    System.out.println("Unsafe counter: " + (++unsafeCounter));
                    System.out.println("Safe counter:   " + safeCounter.incrementAndGet());
                });
            }
        } finally {
            if (service != null) {
                service.shutdown();
                service.awaitTermination(10, TimeUnit.SECONDS);
            }
        }

        System.out.println("Final value - Unsafe counter: " + unsafeCounter);
        System.out.println("Final value - Safe counter:   " + safeCounter.get());
    }
}
