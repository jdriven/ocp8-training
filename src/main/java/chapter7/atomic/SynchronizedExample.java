package chapter7.atomic;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class SynchronizedExample {
    private static final int NR_OF_THREADS = 100;

    private static int unsafeCounter = 0;

    public static void main(String[] args) throws InterruptedException {
        ExecutorService service = null;
        try {
            service = Executors.newFixedThreadPool(NR_OF_THREADS);

            for (int i = 1; i <= NR_OF_THREADS; i++) {
                service.submit(() -> System.out.println("Unsafe counter: " + incrementAndGetCounter()));
            }
        } finally {
            if (service != null) {
                service.shutdown();
                service.awaitTermination(10, TimeUnit.SECONDS);
            }
        }

        System.out.println("Final value - Unsafe counter: " + unsafeCounter);
    }

    private static synchronized int incrementAndGetCounter() {
        return ++unsafeCounter;
    }
}
