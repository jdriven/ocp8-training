package chapter7.executorservice;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

@SuppressWarnings("DuplicatedCode")
public class ExecutorExamples {
    public static void main(String[] args) {
        usingExecute();
        System.out.println();
        usingSubmit();
        System.out.println();
        usingInvokeAll();
        System.out.println();
        usingInvokeAny();
    }

    private static void usingExecute() {
        System.out.println("Using execute");

        ExecutorService service = null;
        try {
            service = Executors.newSingleThreadExecutor();
            System.out.println("Executing tasks");
            service.execute(() -> System.out.println("Executing task #1"));
            service.execute(() -> System.out.println("Executing task #2"));
            service.execute(() -> System.out.println("Executing task #3"));
            service.execute(() -> System.out.println("Executing task #4"));
            service.execute(() -> System.out.println("Executing task #5"));
            System.out.println("Finished executing tasks");
        } finally {
            if (service != null) {
                service.shutdown();
            }
        }
    }

    private static void usingSubmit() {
        System.out.println("Using submit:");

        List<Future<?>> futures = new ArrayList<>();

        ExecutorService service = null;
        try {
            service = Executors.newSingleThreadExecutor();
            System.out.println("Submitting tasks");
            futures.add(service.submit(() -> System.out.println("This is submitted task #1")));
            futures.add(service.submit(() -> System.out.println("This is submitted task #2")));
            futures.add(service.submit(() -> System.out.println("This is submitted task #3")));
            futures.add(service.submit(() -> System.out.println("This is submitted task #4")));
            futures.add(service.submit(() -> System.out.println("This is submitted task #5")));
            System.out.println("Finished submitting tasks");
        } finally {
            if (service != null) {
                service.shutdown();
            }
        }

        for (int i = 0; i < futures.size(); i++) {
            System.out.printf("Submitted task #%d done? %s%n", (i + 1), futures.get(i).isDone());
        }
    }

    private static void usingInvokeAll() {
        ExecutorService service = null;
        try {
            service = Executors.newSingleThreadExecutor();

            System.out.println("Creating tasks");
            List<Callable<Integer>> tasks = new ArrayList<>();
            for (int i = 1; i <= 10; i++) {
                int num = i;
                tasks.add(() -> {
                    System.out.println("Starting task #" + num);
                    return num * num;
                });
            }
            System.out.println("Done tasks");

            System.out.println("Invoking all tasks");
            List<Future<Integer>> results = service.invokeAll(tasks);
            System.out.println("Done invoking all tasks");

            for (Future<Integer> result : results) {
                // result.isDone(); // This is always true after using invokeAll(...)
                System.out.println(result.get());
            }

        } catch (InterruptedException | ExecutionException e) {
            System.out.println("Caught Exception: " + e.getClass());
        } finally {
            if (service != null) {
                service.shutdown();
            }
        }
    }

    private static void usingInvokeAny() {
        ExecutorService service = null;
        try {
            service = Executors.newSingleThreadExecutor();

            System.out.println("Creating tasks");
            List<Callable<Integer>> tasks = new ArrayList<>();
            for (int i = 1; i <= 10; i++) {
                int num = i;
                tasks.add(() -> {
                    System.out.println("Starting task #" + num);
                    return num * num;
                });
            }
            System.out.println("Done tasks");

            System.out.println("Invoking any task");
            Integer result = service.invokeAny(tasks);
            System.out.println("Done invoking any task");

            System.out.println(result);

        } catch (InterruptedException | ExecutionException e) {
            System.out.println("Caught Exception: " + e.getClass());
        } finally {
            if (service != null) {
                service.shutdown();
            }
        }
    }
}
