package chapter7.executorservice;

import java.time.LocalTime;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Scheduling {
    public static void main(String[] args) throws InterruptedException {
        withDelay();
        System.out.println();
        withFixedRate();
        System.out.println();
        withFixedDelay();
    }

    private static void withDelay() throws InterruptedException {
        System.out.println("With delay:");
        ScheduledExecutorService service = null;
        try {
            service = Executors.newSingleThreadScheduledExecutor();

            Runnable task = () -> print("Executing task");

            print("Scheduling task");
            service.schedule(task, 1, TimeUnit.SECONDS);

        } finally {
            if (service != null) {
                service.shutdown();
                service.awaitTermination(10, TimeUnit.SECONDS);
            }
        }
    }

    private static int fixedRateTaskCounter = 0;
    private static void withFixedRate() throws InterruptedException {
        System.out.println("With fixed rate:");
        ScheduledExecutorService service = null;
        try {
            service = Executors.newSingleThreadScheduledExecutor();

            Runnable task = () -> {
                int taskNr = ++fixedRateTaskCounter;
                print("Starting task " + taskNr);
                sleep(500);
                print("Finishing task " + taskNr);
            };

            print("Scheduling task");
            service.scheduleAtFixedRate(task, 1, 1, TimeUnit.SECONDS);

            sleep(5000);
        } finally {
            if (service != null) {
                service.shutdown();
                service.awaitTermination(10, TimeUnit.SECONDS);
            }
        }
    }

    private static int fixedDelayTaskCounter = 0;
    private static void withFixedDelay() throws InterruptedException {
        System.out.println("With fixed delay:");
        ScheduledExecutorService service = null;
        try {
            service = Executors.newSingleThreadScheduledExecutor();

            Runnable task = () -> {
                int taskNr = ++fixedDelayTaskCounter;
                print("Starting task " + taskNr);
                sleep(500);
                print("Finishing task " + taskNr);
            };

            print("Scheduling task");
            service.scheduleWithFixedDelay(task, 1, 1, TimeUnit.SECONDS);

            sleep(5000);
        } finally {
            if (service != null) {
                service.shutdown();
                service.awaitTermination(10, TimeUnit.SECONDS);
            }
        }
    }

    private static void print(String msg) {
        System.out.printf("%s - %s%n", LocalTime.now(), msg);
    }

    private static void sleep(int ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            print("Interrupted");
        }
    }
}
