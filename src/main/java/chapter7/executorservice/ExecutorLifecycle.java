package chapter7.executorservice;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ExecutorLifecycle {

    public static void main(String[] args) throws InterruptedException {
        withShutdown();
        System.out.println();
        withShutdownNow();
    }

    private static void withShutdown() throws InterruptedException {
        System.out.println("With shutdown:");

        Runnable longRunningRunnable = longRunningRunnable();
        Runnable shortRunningRunnable = shortRunningRunnable();

        ExecutorService service = null;
        try {
            service = Executors.newSingleThreadExecutor();
            System.out.println("Created executor");

            System.out.println("Submitting first runnable");
            service.submit(longRunningRunnable);

            System.out.println("Submitting second runnable");
            service.submit(shortRunningRunnable);
        } finally {
            if (service != null) {
                System.out.println("Calling shutdown()");
                service.shutdown();

                System.out.println("Awaiting termination...");
                service.awaitTermination(5, TimeUnit.SECONDS);
                System.out.println("Shutdown complete!");
            }
        }
    }

    private static void withShutdownNow() throws InterruptedException {
        System.out.println("With shutdownNow:");

        Runnable longRunningRunnable = longRunningRunnable();
        Runnable shortRunningRunnable = shortRunningRunnable();

        ExecutorService service = null;
        try {
            service = Executors.newSingleThreadExecutor();
            System.out.println("Created executor");

            System.out.println("Submitting first runnable");
            service.submit(longRunningRunnable);

            System.out.println("Submitting second runnable");
            service.submit(shortRunningRunnable);
        } finally {
            if (service != null) {
                System.out.println("Calling shutdownNow()");
                List<Runnable> notExecutedTasks = service.shutdownNow();
                System.out.println("# Tasks not executed: " + notExecutedTasks.size());

                System.out.println("Awaiting termination...");
                service.awaitTermination(5, TimeUnit.SECONDS);
                System.out.println("Shutdown complete!");
            }
        }
    }

    private static Runnable longRunningRunnable() {
        return () -> {
            System.out.println("Starting long running runnable");
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                System.out.println("Long running runnable interrupted");
            }
            System.out.println("Finished long running runnable");
        };
    }

    private static Runnable shortRunningRunnable() {
        return () -> {
            System.out.println("Starting short running runnable");
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                System.out.println("Short running runnable interrupted");
            }
            System.out.println("Finished short running runnable");
        };
    }
}
