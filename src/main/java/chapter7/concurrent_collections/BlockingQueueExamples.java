package chapter7.concurrent_collections;

import java.time.LocalTime;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;

public class BlockingQueueExamples {

    private static boolean running = true;

    public static void main(String[] args) throws InterruptedException {
        slowProducerFastConsumer();
        System.out.println();
        slowProducerSlowConsumer();
        System.out.println();
        fastProducerSlowConsumer();
        System.out.println();
        fastProducerFastConsumer();
    }

    private static void slowProducerFastConsumer() throws InterruptedException {
        System.out.println("Slow producer, fast consumer:");
        runTest(BlockingQueueExamples::slowProducer, BlockingQueueExamples::fastConsumer);
    }

    private static void slowProducerSlowConsumer() throws InterruptedException {
        System.out.println("Slow producer, slow consumer:");
        runTest(BlockingQueueExamples::slowProducer, BlockingQueueExamples::slowConsumer);
    }

    private static void fastProducerSlowConsumer() throws InterruptedException {
        System.out.println("Fast producer, slow consumer:");
        runTest(BlockingQueueExamples::fastProducer, BlockingQueueExamples::slowConsumer);
    }

    private static void fastProducerFastConsumer() throws InterruptedException {
        System.out.println("Fast producer, fast consumer:");
        runTest(BlockingQueueExamples::fastProducer, BlockingQueueExamples::fastConsumer);
    }

    private static void runTest(
            Function<BlockingQueue<Integer>, Runnable> producerCreator,
            Function<BlockingQueue<Integer>, Runnable> consumerCreator) throws InterruptedException {
        running = true;
        ExecutorService service = null;
        try {
            service = Executors.newFixedThreadPool(2);

            BlockingQueue<Integer> queue = new LinkedBlockingQueue<>(10);

            service.submit(consumerCreator.apply(queue));
            service.submit(producerCreator.apply(queue));

            sleep(5000);
        } finally {
            running = false;
            if (service != null) {
                service.shutdown();
                service.awaitTermination(10, TimeUnit.SECONDS);
            }
        }
    }

    private static Runnable slowProducer(BlockingQueue<Integer> queue) {
        AtomicInteger counter = new AtomicInteger();
        return () -> {
            while (running) {
                sleep(1000);
                int num = counter.incrementAndGet();
                print("Producing " + num);
                if (!queue.offer(num)) {
                    print("QUEUE FULL!");
                }
            }
        };
    }

    private static Runnable fastProducer(BlockingQueue<Integer> queue) {
        AtomicInteger counter = new AtomicInteger();
        return () -> {
            while (running) {
                sleep(100);
                int num = counter.incrementAndGet();
                print("Producing " + num);
                if (!queue.offer(num)) {
                    print("QUEUE FULL!");
                }
            }
        };
    }

    private static Runnable slowConsumer(BlockingQueue<Integer> queue) {
        return () -> {
            while (running) {
                sleep(1000);
                Integer num = queue.poll();
                if (num != null) {
                    print("Consuming " + num);
                }
            }
        };
    }

    private static Runnable fastConsumer(BlockingQueue<Integer> queue) {
        return () -> {
            while (running) {
                sleep(100);
                Integer num = queue.poll();
                if (num != null) {
                    print("Consuming " + num);
                }
            }
        };
    }

    private static void print(String msg) {
        System.out.printf("%s - %s%n", LocalTime.now(), msg);
    }

    private static void sleep(int ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            // Ignore
        }
    }
}
