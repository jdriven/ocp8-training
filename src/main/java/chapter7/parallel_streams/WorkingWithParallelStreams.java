package chapter7.parallel_streams;

import java.util.stream.IntStream;

public class WorkingWithParallelStreams {
    public static void main(String[] args) {

        // Performance gains
        System.out.println("Sequential:");
        long start = System.currentTimeMillis();
        int result = IntStream.range(0, 500)
                .map(i -> {
                    sleep(10);
                    return i * i;
                })
                .sum();
        long duration = System.currentTimeMillis() - start;
        System.out.printf("Result: %d (duration: %d ms)%n", result, duration);

        System.out.println("Parallel:");
        start = System.currentTimeMillis();
        result = IntStream.range(0, 500)
                .parallel()
                .map(i -> {
                    sleep(10);
                    return i * i;
                })
                .sum();
        duration = System.currentTimeMillis() - start;
        System.out.printf("Result: %d (duration: %d ms)%n", result, duration);
    }

    private static void sleep(int ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            // Ignore
        }
    }
}
