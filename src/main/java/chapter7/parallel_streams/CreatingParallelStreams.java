package chapter7.parallel_streams;

import java.util.Arrays;
import java.util.stream.Stream;

@SuppressWarnings("SimplifyStreamApiCallChains")
public class CreatingParallelStreams {
    public static void main(String[] args) {

        // Sequential stream
        Stream<Integer> sequentialStream = Arrays.asList(1, 2, 3, 4, 5).stream();
        System.out.println("Sequential stream is parallel? " + sequentialStream.isParallel());
        sequentialStream.forEach(i -> System.out.print(i + " "));

        System.out.println(System.lineSeparator());

        // Parallel stream, method 1
        Stream<Integer> parallelStream1 = Arrays.asList(1, 2, 3, 4, 5).stream().parallel();
        System.out.println("Parallel stream 1 is parallel? " + parallelStream1.isParallel());
        parallelStream1.forEach(i -> System.out.print(i + " "));

        System.out.println(System.lineSeparator());

        // Parallel stream, method 2
        Stream<Integer> parallelStream2 = Arrays.asList(1, 2, 3, 4, 5).parallelStream();
        System.out.println("Parallel stream 2 is parallel? " + parallelStream2.isParallel());
        parallelStream2.forEach(i -> System.out.print(i + " "));
    }
}
