package chapter7.threads;

import java.util.ArrayList;
import java.util.List;

public class ThreadOrdering {
    public static void main(String[] args) {
        List<Thread> threads = new ArrayList<>();

        for (int i = 1; i <= 10; i++) {
            String threadName = "Thread " + i;
            threads.add(new Thread(() -> {
                System.out.println(threadName + " started");
                sleep(100);
                System.out.println(threadName + " ended");
            }));
        }

        threads.forEach(Thread::start);
    }

    private static void sleep(int ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            // Ignore
        }
    }
}
