package chapter7.threads;

public class RunnableExamples {
    public static void main(String[] args) throws InterruptedException {
        runnableInCustomClass();
        System.out.println();
        runnableInAnonymousInnerClass();
        System.out.println();
        runnableWithLambda();
    }

    private static void runnableInCustomClass() throws InterruptedException {
        System.out.println("Runnable in custom Class:");
        FactorialCalculation calc = new FactorialCalculation(10);

        Thread thread = new Thread(calc);
        thread.start();
        thread.join();
    }

    private static void runnableInAnonymousInnerClass() throws InterruptedException {
        System.out.println("Runnable in anonymous inner Class:");

        long num = 5;
        Runnable squareCalculator = new Runnable() {
            @Override
            public void run() {
                long result = num * num;
                System.out.printf("%1$d * %1$d = %2$d%n", num, result);
            }
        };
        Thread thread = new Thread(squareCalculator);
        thread.start();
        thread.join();
    }

    private static void runnableWithLambda() throws InterruptedException {
        System.out.println("Runnable with a lambda:");

        double num = 25;
        Thread thread = new Thread(() -> {
            double result = Math.sqrt(num);
            System.out.printf("sqrt(%.0f) = %.0f%n", num, result);
        });
        thread.start();
        thread.join();
    }
}

class FactorialCalculation implements Runnable {

    private final int num;

    FactorialCalculation(int num) {
        this.num = num;
    }

    @Override
    public void run() {
        long result = num;
        for (int i = num - 1; i > 0; i--) {
            result *= i;
        }
        System.out.printf("Factorial of %d = %d%n", num, result);
    }
}
