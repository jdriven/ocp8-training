package chapter7.quiz;

public class ThreadQuestion extends Thread {
    String msg = "default";

    public ThreadQuestion(String s) {
        msg = s;
    }

    public void run() {
        System.out.println(msg);
    }

    public static void main(String[] args) {
        new ThreadQuestion("String1").run();
        new ThreadQuestion("String2").run();
        System.out.println("end");
    }
}
