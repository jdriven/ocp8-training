package chapter7.managing_concurrency;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.Callable;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class CyclicBarrierExample {
    private static final int NR_OF_THREADS = 4;

    public static void main(String[] args) throws InterruptedException {
        withoutCyclicBarrier();
        System.out.println();
        withCyclicBarrier();
    }

    private static void withoutCyclicBarrier() throws InterruptedException {
        System.out.println("Without cyclic barrier:");

        ExecutorService service = null;
        try {
            service = Executors.newFixedThreadPool(NR_OF_THREADS);

            for (int i = 0; i < NR_OF_THREADS; i++) {
                service.submit(new TaskRunner("Job " + (i + 1), null));
            }
        } finally {
            if (service != null) {
                service.shutdown();
                service.awaitTermination(10, TimeUnit.SECONDS);
            }
        }
    }

    private static void withCyclicBarrier() throws InterruptedException {
        System.out.println("With cyclic barrier:");

        ExecutorService service = null;
        try {
            service = Executors.newFixedThreadPool(NR_OF_THREADS);
            CyclicBarrier cyclicBarrier = new CyclicBarrier(NR_OF_THREADS, () -> System.out.println("Barrier reached!"));

            for (int i = 0; i < NR_OF_THREADS; i++) {
                service.submit(new TaskRunner("Job " + (i + 1), cyclicBarrier));
            }
        } finally {
            if (service != null) {
                service.shutdown();
                service.awaitTermination(10, TimeUnit.SECONDS);
            }
        }
    }
}

class TaskRunner implements Callable<Void> {

    private String name;
    private CyclicBarrier cyclicBarrier;

    TaskRunner(String name, CyclicBarrier cyclicBarrier) {
        this.name = name;
        this.cyclicBarrier = cyclicBarrier;
    }

    @Override
    public Void call() throws BrokenBarrierException, InterruptedException {
        doTask1();
        awaitCyclicBarrier();
        doTask2();
        awaitCyclicBarrier();
        doTask3();
        awaitCyclicBarrier();
        doTask4();
        return null;
    }

    private void doTask1() {
        System.out.println(name + " - Performing task 1");
    }

    private void doTask2() {
        System.out.println(name + " - Performing task 2");
    }

    private void doTask3() {
        System.out.println(name + " - Performing task 3");
    }

    private void doTask4() {
        System.out.println(name + " - Performing task 4");
    }

    private void awaitCyclicBarrier() throws BrokenBarrierException, InterruptedException {
        if (cyclicBarrier != null) {
            cyclicBarrier.await();
        }
    }
}
