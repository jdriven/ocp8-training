package chapter7.managing_concurrency;

import java.util.Random;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;
import java.util.concurrent.atomic.AtomicInteger;

public class ForkJoinExample {
    public static void main(String[] args) {
        Integer[] randomNumbers = createRandomIntegerArray();

        long start = System.currentTimeMillis();

        ForkJoinTask<Integer> task = new FindMaximumTask(randomNumbers);
        ForkJoinPool pool = new ForkJoinPool();
        Integer max = pool.invoke(task);

        long duration = System.currentTimeMillis() - start;

        System.out.printf("Maximum: %d found in %d ms%n", max, duration);
        System.out.println("Number of tasks created: " + FindMaximumTask.getObjectCount());
    }

    private static Integer[] createRandomIntegerArray() {
        Random random = new Random();
        Integer[] randomNumbers = new Integer[1_000_000];
        for (int i = 0; i < randomNumbers.length; i++) {
            int nextInt = random.nextInt();
            randomNumbers[i] = nextInt;
        }
        return randomNumbers;
    }
}

class FindMaximumTask extends RecursiveTask<Integer> {

    private static AtomicInteger objectCount = new AtomicInteger();

    private final Integer[] numbers;
    private final int start;
    private final int end;

    FindMaximumTask(Integer[] numbers) {
        this(numbers, 0, numbers.length - 1);
    }

    private FindMaximumTask(Integer[] numbers, int start, int end) {
        this.numbers = numbers;
        this.start = start;
        this.end = end;
        objectCount.incrementAndGet();
    }

    static int getObjectCount() {
        return objectCount.get();
    }

    @Override
    protected Integer compute() {
        if (end - start == 1) {
            return Math.max(numbers[start], numbers[end]);
        } else {
            int middle = start + ((end - start) / 2);
            RecursiveTask<Integer> otherTask = new FindMaximumTask(numbers, start, middle);
            otherTask.fork();

            return Math.max(new FindMaximumTask(numbers, middle, end).compute(), otherTask.join());
        }
    }
}
