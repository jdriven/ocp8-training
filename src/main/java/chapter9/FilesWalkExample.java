package chapter9;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;


import utils.FileUtils;

public class FilesWalkExample {
    public static void main(String[] args) throws URISyntaxException, IOException {

        Path sourcePath = Paths.get(FilesWalkExample.class.getResource("/").toURI());
        System.out.println("Source path: " + sourcePath);
        System.out.println();

        walk(sourcePath);
        System.out.println();
        find(sourcePath);
        System.out.println();
        list(sourcePath);
        System.out.println();
        lines();
    }

    private static void walk(final Path sourcePath) throws IOException {
        System.out.println("Files.walk():");
        Files.walk(sourcePath)
                .filter(p -> !Files.isDirectory(p))                         // No directories
                .filter(p -> p.toString().endsWith(".class"))               // Class files only
                .map(Path::getFileName)                                     // Convert to file name path only
                .map(Path::toString)                                        // Convert Path to String
                .map(name -> name.replace(".class", ""))    // Remove '.class' extension
                .forEach(System.out::println);
    }

    private static void find(final Path sourcePath) throws IOException {
        System.out.println("Files.find():");
        Files.find(sourcePath, 10, (p, a) -> p.toString().endsWith(".class"))
                .map(Path::getFileName)
                .map(Path::toString)
                .map(name -> name.replace(".class", ""))
                .forEach(System.out::println);
    }

    private static void list(final Path sourcePath) throws IOException {
        System.out.println("Files.list():");
        Files.list(sourcePath)
                .forEach(System.out::println);
    }

    private static void lines() throws IOException {
        System.out.println("lines():");
        String text = String.join(System.lineSeparator(), "line 1", "line 2", "line 3", "line 4", "line 5");
        Path textFile = FileUtils.createTempFile(text).toPath();

        Files.lines(textFile).forEach(System.out::println);
    }
}
