package chapter9;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;

public class PathExamples {
    public static void main(String[] args) throws IOException {

        Path absolutePath = Paths.get("C:/tmp/something/file.txt"); // Change to Unix style paths if you're running Linux/Mac
        System.out.println("Absolute path: " + absolutePath);
        System.out.println("Is absolute? " + absolutePath.isAbsolute());
        Path absolutePathFromFileSystem = FileSystems.getDefault().getPath("C:/tmp/something/file.txt");
        System.out.println("Could also use FileSystem? " + (absolutePath.equals(absolutePathFromFileSystem)));
        System.out.println("Name count: " + absolutePath.getNameCount());
        for (int i = 0; i < absolutePath.getNameCount(); i++) {
            System.out.printf("   Name %d: %s%n", i, absolutePath.getName(i));
        }
        System.out.println("File name: "+ absolutePath.getFileName());
        System.out.println("Parent: "+ absolutePath.getParent());
        System.out.println("Root: "+ absolutePath.getRoot());
        System.out.println("Root parent: "+ absolutePath.getRoot().getParent());
        System.out.println("Sub path from 1 to 3: " + absolutePath.subpath(1, 3));

        System.out.println();

        Path relativePath = Paths.get("..\\files\\file.txt");
        System.out.println("Relative path: " + relativePath);
        System.out.println("Is absolute? " + relativePath.isAbsolute());
        System.out.println("To absolute path: " + relativePath.toAbsolutePath());

        System.out.println();

        Path pathFromParts = Paths.get("C:", "tmp", "file.txt");
        System.out.println("Path from parts: " + pathFromParts);

        System.out.println();

        Path path1 = Paths.get("tmp", "sub1", "sub2", "subA", "subB", "file.txt");
        System.out.println("Path 1: " + path1);
        Path path2 = Paths.get("tmp", "sub1", "sub2", "subX", "subY", "file.txt");
        System.out.println("Path 2: " + path2);
        System.out.println("Path1 to path2: " + path1.relativize(path2));
        System.out.println("Path2 to path1: " + path2.relativize(path1));

        System.out.println();

        Path path3 = Paths.get("some", "base", "path");
        System.out.println("Path 3: " + path3);
        Path path4 = Paths.get("some-file.txt");
        System.out.println("Path 4: " + path4);
        System.out.println("Path3.resolve(path4): " + path3.resolve(path4));

        Path path5 = Paths.get("C:", "base", "dir1", "file1.txt");
        System.out.println("Path 5: " + path5);
        Path path6 = Paths.get("C:", "base", "dir2", "file2.txt");
        System.out.println("Path 6: " + path6);
        Path relativized = path5.resolve(path5.relativize(path6));
        System.out.println("Relativized & resolved: " + relativized);
        System.out.println("Normalized: " + relativized.normalize());

        System.out.println();

        Path tempDir = Paths.get(System.getProperty("java.io.tmpdir"));
        System.out.println("Temp dir: " + tempDir.toRealPath());
    }
}
