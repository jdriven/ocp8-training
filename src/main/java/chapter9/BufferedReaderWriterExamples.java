package chapter9;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;


import utils.FileUtils;

public class BufferedReaderWriterExamples {
    public static void main(String[] args) throws IOException {
        Path tmpFile = FileUtils.createTempFile().toPath();

        bufferedWriter(tmpFile);
        System.out.println();
        bufferedReader(tmpFile);
    }

    private static void bufferedWriter(Path tmpFile) throws IOException {
        byte[] content = new byte[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
        System.out.println("BufferedWriter:");

        try (BufferedWriter writer = Files.newBufferedWriter(tmpFile, Charset.defaultCharset())) {
            System.out.println("Start writing");
            for (int i = 1; i <= 5; i++) {
                String line = "Line " + i;
                System.out.println("Writing: " + line);
                writer.write(line);
                writer.newLine();
            }
            System.out.println("Done writing");
        }
    }

    private static void bufferedReader(Path tmpFile) throws IOException {
        System.out.println("BufferedReader:");

        System.out.println("Start reading");
        try (BufferedReader bufferedReader = Files.newBufferedReader(tmpFile, Charset.defaultCharset())) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                System.out.println("Read line: " + line);
            }
        }
        System.out.println("Done reading");
        System.out.println();

        System.out.println("Alternatively, read everything at once:");
        Files.readAllLines(tmpFile).forEach(System.out::println);
    }
}
