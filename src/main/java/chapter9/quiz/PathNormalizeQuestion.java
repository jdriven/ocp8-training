package chapter9.quiz;

import java.nio.file.Path;
import java.nio.file.Paths;

public class PathNormalizeQuestion {
    public static void main(String[] args) {
        Path p1 = Paths.get("c:\\personal\\.\\photos\\..\\readme.txt");
        // Path p1 = Paths.get("/personal/./photos//..//readme.txt"); // Use this line if you're on a UNIX based system
        Path p2 = p1.normalize();
        System.out.println(p2);

        /*
            What is the output of the code above?
            1. readme.txt
            2. c:\personal\photos\readme.txt
            3. c:\personal\readme.txt
            4. c:\photos\readme.txt
        */
    }
}
