package chapter9.quiz;

public class FindFileQuestion {

        /* Given thats c:\temp\pathtest is a directory that contains serveral directories.
           Each sub directory contains several files but there is exactly one regular file
           named test.txt within the whole directory structure.

           Which of the given options can be inserted in the code below so that it will print
           the complete path of test.txt?


        try {
            Stream<Path> s = null;
            // INSERT CODE HERE
            s.forEach(System.out::println);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }


        1. s = Files.list(Paths.get("c:\\temp\\pathtest\\**\\test.txt"));
        2. s = Files.walk(Paths.get("c:\\temp\\pathtest"), "test.txt");
        3. s = Files.find(Paths.get("c:\\temp\\pathtest"), Integer.MAX_VALUE, (p, a) -> p.endsWith("test.txt") && a.isRegularFile());
        4. s = Files.find(Paths.get("test.txt"));
        5. s = Files.list(Paths.get("c:\\temp\\pathtest"), (p, a) -> p.endsWith("test.txt") && a.isRegularFile());
        6. s = Files.walk(Paths.get("c:\\temp\\pathtest"), (p, a) -> p.endsWith("test.txt") && a.isRegularFile());
        */

}
