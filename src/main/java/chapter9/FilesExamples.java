package chapter9;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;


import utils.FileUtils;

public class FilesExamples {
    public static void main(String[] args) throws IOException {
        Path tempPath = FileUtils.createTempFile().toPath();

        System.out.println("Attributes: " + Files.readAttributes(tempPath, "*"));
        System.out.println("Temp file exists? " + Files.exists(tempPath));
        System.out.println("Non-existent file exists? " + Files.exists(Paths.get("98r3098vt9438myvt4iuthiuhergdser"))); // What are the odds?

        System.out.println();

        Path sameFile1 = Paths.get("tmp", "samefile.txt");
        Path sameFile2 = Paths.get("tmp", "samefile.txt");
        System.out.printf("Is '%s' the same file as '%s'? %s%n", sameFile1, sameFile2, Files.isSameFile(sameFile1, sameFile2));

        System.out.println();

        Path tmpDir = FileUtils.createTempDir().toPath();
        Path subdirectory = tmpDir.resolve("subdir");
        System.out.println("Creating: " + subdirectory);
        Files.createDirectory(subdirectory);
        System.out.println("Succeeded? " + Files.exists(subdirectory));

        Path subdirectories = tmpDir.resolve(Paths.get("subdir", "subsubdir", "subsubdir"));
        System.out.println("Creating: " + subdirectories);
        Files.createDirectories(subdirectories);
        System.out.println("Succeeded? " + Files.exists(subdirectories));

        System.out.println();

        String text = "some text";
        Path tmpFile = FileUtils.createTempFile(text).toPath();
        Path copyOfTmpFile = FileUtils.createTempFile().toPath();
        System.out.printf("Copying file '%s' to '%s'%n", tmpFile, copyOfTmpFile);
        Files.copy(tmpFile, copyOfTmpFile, StandardCopyOption.REPLACE_EXISTING);
        System.out.println("Content of copy: " + FileUtils.getContentAsString(copyOfTmpFile.toFile()));

        Path movedTmpFile = tmpFile.getParent().resolve("moved-" + tmpFile.getFileName());
        System.out.printf("Moving file '%s' to '%s'%n", tmpFile, movedTmpFile);
        Files.move(tmpFile, movedTmpFile);
        System.out.println("Move successful? " + (Files.exists(movedTmpFile) && !Files.exists(tmpFile)));

        System.out.println();

        System.out.println("Deleting files");
        System.out.println("tmpFile deleted? " + Files.deleteIfExists(tmpFile));
        System.out.println("movedTmpFile deleted? " + Files.deleteIfExists(movedTmpFile));
        Files.delete(copyOfTmpFile);
    }
}
