package chapter5.i18n;

import java.util.Locale;
import java.util.ResourceBundle;

public class ClassResourceBundles {
    public static void main(String[] args) {
        Locale localeEn = Locale.ENGLISH;
        Locale localeEnUS = Locale.US;
        Locale localeEnGB = Locale.UK;
        Locale localeJaJP = Locale.JAPAN;

        System.out.println("Default:      " + ResourceBundle.getBundle("chapter5.i18n.resources.Language").getObject("language"));
        System.out.println("Locale en:    " + ResourceBundle.getBundle("chapter5.i18n.resources.Language", localeEn).getObject("language"));
        System.out.println("Locale en_US: " + ResourceBundle.getBundle("chapter5.i18n.resources.Language", localeEnUS).getObject("language"));
        System.out.println("Locale en_GB: " + ResourceBundle.getBundle("chapter5.i18n.resources.Language", localeEnGB).getObject("language"));
        System.out.println("Locale ja_JP: " + ResourceBundle.getBundle("chapter5.i18n.resources.Language", localeJaJP).getObject("language"));
    }
}
