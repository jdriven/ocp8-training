package chapter5.i18n;

import java.util.Locale;
import java.util.ResourceBundle;

public class ResourceBundles {
    public static void main(String[] args) {
        Locale localeEn = Locale.ENGLISH;
        Locale localeEnUS = Locale.US;
        Locale localeEnGB = Locale.UK;
        Locale localeJaJP = Locale.JAPAN;

        System.out.println("Default:      " + ResourceBundle.getBundle("chapter5.i18n.resources.words").getString("word"));
        System.out.println("Locale en:    " + ResourceBundle.getBundle("chapter5.i18n.resources.words", localeEn).getString("word"));
        System.out.println("Locale en_US: " + ResourceBundle.getBundle("chapter5.i18n.resources.words", localeEnUS).getString("word"));
        System.out.println("Locale en_GB: " + ResourceBundle.getBundle("chapter5.i18n.resources.words", localeEnGB).getString("word"));
        System.out.println("Locale ja_JP: " + ResourceBundle.getBundle("chapter5.i18n.resources.words", localeJaJP).getString("word"));
    }
}
