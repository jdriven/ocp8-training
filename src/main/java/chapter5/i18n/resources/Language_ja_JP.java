package chapter5.i18n.resources;

import java.util.ListResourceBundle;

public class Language_ja_JP extends ListResourceBundle {
    @Override
    protected Object[][] getContents() {
        return new Object[][] {
                { "language", "Japanese" }
        };
    }
}
