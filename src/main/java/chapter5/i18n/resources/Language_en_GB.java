package chapter5.i18n.resources;

import java.util.ListResourceBundle;

public class Language_en_GB extends ListResourceBundle {
    @Override
    protected Object[][] getContents() {
        return new Object[][] {
                { "language", "English (UK)" }
        };
    }
}
