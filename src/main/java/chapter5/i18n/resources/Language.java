package chapter5.i18n.resources;

import java.util.ListResourceBundle;

public class Language extends ListResourceBundle {
    @Override
    protected Object[][] getContents() {
        return new Object[][] {
                { "language", "Default" }
        };
    }
}
