package chapter5.i18n;

import java.util.Locale;

public class Locales {
    public static void main(String[] args) {
        System.out.println("Netherlands: " + new Locale("nl", "NL"));
        System.out.println("Dutch language: " + new Locale("nl"));
        System.out.println("Germany: " + Locale.GERMANY);
        System.out.println("USA: " + new Locale.Builder().setLanguage("en").setRegion("US").build());
        System.out.println("Default locale: " + Locale.getDefault());
        Locale.setDefault(Locale.JAPAN);
        System.out.println("Default locale (altered): " + Locale.getDefault());
    }
}
