package chapter5.datetime;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class DaylightSavingsTime {
    public static void main(String[] args) {
        summer();
        System.out.println();
        winter();
    }

    private static void summer() {
        System.out.println("Summer time (March 31st, 2019):");
        LocalDate date = LocalDate.of(2019, Month.MARCH, 31);
        LocalTime timeBeforeSummerTime = LocalTime.of(1, 59);

        ZonedDateTime beforeSummerTime = ZonedDateTime.of(date, timeBeforeSummerTime, ZoneId.of("Europe/Amsterdam"));
        System.out.println("Before:        " + beforeSummerTime);
        System.out.println("  +1 minute:   " + beforeSummerTime.plusMinutes(1));
        System.out.println("Time at 02:30: " + ZonedDateTime.of(date, LocalTime.of(2, 30), ZoneId.of("Europe/Amsterdam")));
    }

    private static void winter() {
        System.out.println("Winter time (October 27th, 2019):");
        LocalDate date = LocalDate.of(2019, 10, 27);
        LocalTime timeBeforeWinterTime = LocalTime.of(1, 59);

        ZonedDateTime beforeWinterTime = ZonedDateTime.of(date, timeBeforeWinterTime, ZoneId.of("Europe/Amsterdam"));
        System.out.println("Before:         " + beforeWinterTime);
        System.out.println("  +1 hour:      " + beforeWinterTime.plusHours(1));
        System.out.println("  +2 hours:     " + beforeWinterTime.plusHours(2));

        // There are 2 ZonedDateTime instances possible for the time between 02:00 and 03:00 within time zones with Daylight Savings Time
        ZonedDateTime transitionTime1 = ZonedDateTime.of(date, LocalTime.of(1, 30), ZoneId.of("Europe/Amsterdam")).plusHours(1);
        ZonedDateTime transitionTime2 = ZonedDateTime.of(date, LocalTime.of(3, 30), ZoneId.of("Europe/Amsterdam")).minusHours(1);
        System.out.println("01:30 + 1 hour: " + transitionTime1 + " (epoch seconds: " + transitionTime1.toEpochSecond() + ")");
        System.out.println("03:30 - 1 hour: " + transitionTime2 + " (epoch seconds: " + transitionTime2.toEpochSecond() + ")");
    }
}
