package chapter5.datetime;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Period;

@SuppressWarnings("DuplicatedCode")
public class PeriodsAndDurations {
    public static void main(String[] args) {
        periods();
        System.out.println();
        durations();
        System.out.println();
        periodsAndDurationsCombined();
    }

    private static void periods() {
        System.out.println("Periods:");

        System.out.println(Period.ofDays(1));
        System.out.println(Period.ofWeeks(1));
        System.out.println(Period.ofMonths(1));
        System.out.println(Period.ofYears(1));
        System.out.println(Period.of(1, 2, 3));

        LocalDate now = LocalDate.now();
        System.out.println(now);
        System.out.println(now.plus(Period.ofDays(1)));
        System.out.println(now.plus(Period.ofWeeks(1)));
        System.out.println(now.plus(Period.ofMonths(1)));
        System.out.println(now.plus(Period.ofYears(1)));
        System.out.println(now.plus(Period.of(1, 2, 3)));

        // System.out.println(LocalTime.now().plus(Period.ofYears(1))); // Throws UnsupportedTemporalTypeException!
    }

    private static void durations() {
        System.out.println("Durations:");

        System.out.println(Duration.ofNanos(1));
        System.out.println(Duration.ofMillis(1));
        System.out.println(Duration.ofSeconds(1));
        System.out.println(Duration.ofSeconds(1, 1));
        System.out.println(Duration.ofMinutes(1));
        System.out.println(Duration.ofHours(1));
        System.out.println(Duration.ofDays(1));

        LocalTime now = LocalTime.now();
        System.out.println(now);
        System.out.println(now.plus(Duration.ofNanos(1)));
        System.out.println(now.plus(Duration.ofMillis(1)));
        System.out.println(now.plus(Duration.ofSeconds(1)));
        System.out.println(now.plus(Duration.ofSeconds(1, 1)));
        System.out.println(now.plus(Duration.ofMinutes(1)));
        System.out.println(now.plus(Duration.ofHours(1)));
        System.out.println(now.plus(Duration.ofDays(1)));
        System.out.println(now.plus(Duration.ofDays(1)) == now.minus(Duration.ofDays(1))); // LocalTime contains no day info!

        // Duration converts everything to a number of seconds (+ nanos), so even though Duration.ofDays() implies a
        // number of days, you cannot do arithmetic with it on a LocalDate!
        // System.out.println(LocalDate.now().plus(Duration.ofDays(1))); // Throws UnsupportedTemporalTypeException!
    }

    private static void periodsAndDurationsCombined() {
        System.out.println("Combined:");

        LocalDateTime now = LocalDateTime.now();
        System.out.println(now.plus(Duration.ofNanos(1)));
        System.out.println(now.plus(Duration.ofMillis(1)));
        System.out.println(now.plus(Duration.ofSeconds(1)));
        System.out.println(now.plus(Duration.ofSeconds(1, 1)));
        System.out.println(now.plus(Duration.ofMinutes(1)));
        System.out.println(now.plus(Duration.ofHours(1)));
        System.out.println(now.plus(Duration.ofDays(1)));
        System.out.println(now.plus(Duration.ofDays(1)) != now.minus(Duration.ofDays(1))); // LocalTime contains no day info!

        System.out.println(now.plus(Period.ofDays(1)));
        System.out.println(now.plus(Period.ofWeeks(1)));
        System.out.println(now.plus(Period.ofMonths(1)));
        System.out.println(now.plus(Period.ofYears(1)));
        System.out.println(now.plus(Period.of(1, 2, 3)));

    }
}
