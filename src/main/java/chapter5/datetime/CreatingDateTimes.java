package chapter5.datetime;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.stream.Collectors;

public class CreatingDateTimes {
    public static void main(String[] args) {
        localDate();
        System.out.println();
        localTime();
        System.out.println();
        localDateTime();
        System.out.println();
        zonedDateTime();
        System.out.println();
        instant();
        System.out.println();
        zoneIds();
    }

    private static void localDate() {
        System.out.println("LocalDate:");
        // System.out.println(new LocalDate()); // Does not compile!
        System.out.println(LocalDate.now());
        System.out.println(LocalDate.of(2019, 8, 6));
        System.out.println(LocalDate.of(2019, Month.AUGUST, 6));
        // System.out.println(LocalDate.of(2019, Month.AUGUST, 50)); // Throws DateTimeException!
    }

    private static void localTime() {
        System.out.println("LocalTime:");
        // System.out.println(new LocalTime()); // Does not compile!
        System.out.println(LocalTime.now());
        System.out.println(LocalTime.of(18, 30));
        System.out.println(LocalTime.of(18, 30, 10));
        System.out.println(LocalTime.of(18, 30, 10, 123_000_000));
        // System.out.println(LocalTime.of(28, 30, 10, 123_000_000)); // Throws DateTimeException!
    }

    private static void localDateTime() {
        System.out.println("LocalDateTime:");
        // System.out.println(new LocalDateTime()); // Does not compile!
        System.out.println(LocalDateTime.now());
        System.out.println(LocalDateTime.of(LocalDate.now(), LocalTime.now()));
        System.out.println(LocalDateTime.of(2019, 8, 6, 18, 30));
        System.out.println(LocalDateTime.of(2019, 8, 6, 18, 30, 10));
        System.out.println(LocalDateTime.of(2019, 8, 6, 18, 30, 10, 123_000_000));
        System.out.println(LocalDateTime.of(2019, Month.AUGUST, 6, 18, 30));
        System.out.println(LocalDateTime.of(2019, Month.AUGUST, 6, 18, 30, 10));
        System.out.println(LocalDateTime.of(2019, Month.AUGUST, 6, 18, 30, 10, 123_000_000));
        // System.out.println(LocalDateTime.of(2019, Month.AUGUST, 36, 18, 30, 10, 123_000_000)); // Throws DateTimeException!
    }

    private static void zonedDateTime() {
        System.out.println("ZonedDateTime:");
        // System.out.println(new ZonedDateTime()); // Does not compile!
        System.out.println(ZonedDateTime.now());
        System.out.println(ZonedDateTime.of(LocalDateTime.now(), ZoneId.of("America/New_York")));
        System.out.println(ZonedDateTime.of(LocalDate.now(), LocalTime.now(), ZoneId.of("Asia/Tokyo")));
        System.out.println(ZonedDateTime.of(2019, 8, 6, 18, 30, 0, 0, ZoneId.of("Europe/Amsterdam")));
        // System.out.println(ZonedDateTime.of(2019, 28, 6, 18, 30, 0, 0, ZoneId.of("Europe/Amsterdam"))); // Throws DateTimeException!
    }

    private static void instant() {
        System.out.println("Instant:");
        // System.out.println(new Instant()); // Does not compile!
        System.out.println(Instant.now());
        System.out.println(Instant.ofEpochMilli(0));
        System.out.println(Instant.ofEpochSecond(1));
        System.out.println(ZonedDateTime.now().toInstant()); // Converts from the zone in ZonedDateTime to UTC
        System.out.println(LocalDateTime.now().toInstant(ZoneOffset.UTC)); // Uses the local time, and pastes UTC timezone, no conversion!
    }

    private static void zoneIds() {
        System.out.println("Zone Ids:");
        System.out.println(ZoneId.getAvailableZoneIds().stream().sorted().collect(Collectors.toList()));
    }
}
