package chapter5.datetime;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

@SuppressWarnings("DuplicatedCode")
public class ManipulatingDateTimes {
    public static void main(String[] args) {
        localDate();
        System.out.println();
        localTime();
        System.out.println();
        localDateTime();
        System.out.println();
        zonedDateTime();
        System.out.println();
        instant();
    }

    private static void localDate() {
        LocalDate localDate = LocalDate.now();

        System.out.println("LocalDate:");
        System.out.println("-1 year:   " + localDate.minusYears(1));
        System.out.println("-1 month:  " + localDate.minusMonths(1));
        System.out.println("-1 week:   " + localDate.minusWeeks(1));
        System.out.println("-1 day:    " + localDate.minusDays(1));
        System.out.println("Now:       " + localDate);
        System.out.println("+1 day:    " + localDate.plusDays(1));
        System.out.println("+1 week:   " + localDate.plusWeeks(1));
        System.out.println("+1 month:  " + localDate.plusMonths(1));
        System.out.println("+1 year:   " + localDate.plusYears(1));
    }

    private static void localTime() {
        LocalTime localTime = LocalTime.now();

        System.out.println("LocalTime:");
        System.out.println("-1 hour:   " + localTime.minusHours(1));
        System.out.println("-1 minute: " + localTime.minusMinutes(1));
        System.out.println("-1 second: " + localTime.minusSeconds(1));
        System.out.println("-1 nano:   " + localTime.minusNanos(1));
        System.out.println("Now:       " + localTime);
        System.out.println("+1 nano:   " + localTime.plusNanos(1));
        System.out.println("+1 second: " + localTime.plusSeconds(1));
        System.out.println("+1 minute: " + localTime.plusMinutes(1));
        System.out.println("+1 hour:   " + localTime.plusHours(1));
    }

    private static void localDateTime() {
        LocalDateTime localDateTime = LocalDateTime.now();

        System.out.println("LocalDateTime:");
        System.out.println("-1 year:   " + localDateTime.minusYears(1));
        System.out.println("-1 month:  " + localDateTime.minusMonths(1));
        System.out.println("-1 week:   " + localDateTime.minusWeeks(1));
        System.out.println("-1 day:    " + localDateTime.minusDays(1));
        System.out.println("-1 hour:   " + localDateTime.minusHours(1));
        System.out.println("-1 minute: " + localDateTime.minusMinutes(1));
        System.out.println("-1 second: " + localDateTime.minusSeconds(1));
        System.out.println("-1 nano:   " + localDateTime.minusNanos(1));
        System.out.println("Now:       " + localDateTime);
        System.out.println("+1 nano:   " + localDateTime.plusNanos(1));
        System.out.println("+1 second: " + localDateTime.plusSeconds(1));
        System.out.println("+1 minute: " + localDateTime.plusMinutes(1));
        System.out.println("+1 hour:   " + localDateTime.plusHours(1));
        System.out.println("+1 day:    " + localDateTime.plusDays(1));
        System.out.println("+1 week:   " + localDateTime.plusWeeks(1));
        System.out.println("+1 month:  " + localDateTime.plusMonths(1));
        System.out.println("+1 year:   " + localDateTime.plusYears(1));
    }

    private static void zonedDateTime() {
        ZonedDateTime zonedDateTime = ZonedDateTime.now(ZoneId.of("Europe/Amsterdam"));

        System.out.println("ZonedDateTime:");
        System.out.println("-1 year:   " + zonedDateTime.minusYears(1));
        System.out.println("-1 month:  " + zonedDateTime.minusMonths(1));
        System.out.println("-1 week:   " + zonedDateTime.minusWeeks(1));
        System.out.println("-1 day:    " + zonedDateTime.minusDays(1));
        System.out.println("-1 hour:   " + zonedDateTime.minusHours(1));
        System.out.println("-1 minute: " + zonedDateTime.minusMinutes(1));
        System.out.println("-1 second: " + zonedDateTime.minusSeconds(1));
        System.out.println("-1 nano:   " + zonedDateTime.minusNanos(1));
        System.out.println("Now:       " + zonedDateTime);
        System.out.println("+1 nano:   " + zonedDateTime.plusNanos(1));
        System.out.println("+1 second: " + zonedDateTime.plusSeconds(1));
        System.out.println("+1 minute: " + zonedDateTime.plusMinutes(1));
        System.out.println("+1 hour:   " + zonedDateTime.plusHours(1));
        System.out.println("+1 day:    " + zonedDateTime.plusDays(1));
        System.out.println("+1 week:   " + zonedDateTime.plusWeeks(1));
        System.out.println("+1 month:  " + zonedDateTime.plusMonths(1));
        System.out.println("+1 year:   " + zonedDateTime.plusYears(1));
    }

    private static void instant() {
        Instant instant = Instant.now();

        System.out.println("-1 second: " + instant.minusSeconds(1));
        System.out.println("-1 milli:  " + instant.minusMillis(1));
        System.out.println("-1 nano:   " + instant.minusNanos(1));
        System.out.println("Now:       " + instant);
        System.out.println("+1 nano:   " + instant.plusNanos(1));
        System.out.println("+1 milli:  " + instant.plusMillis(1));
        System.out.println("+1 second: " + instant.plusSeconds(1));
    }
}
