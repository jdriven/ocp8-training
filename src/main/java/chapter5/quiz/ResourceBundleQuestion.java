package chapter5.quiz;

import java.util.Locale;
import java.util.ResourceBundle;

public class ResourceBundleQuestion {
    public static void main(String[] args) {

        Locale.setDefault(new Locale("fr", "CA"));
        ResourceBundle rb = ResourceBundle.getBundle("chapter5.quiz.appmessages", new Locale("jp", "JP"));
        String msg = rb.getString("greetings");
        System.out.println(msg);

    }
}
