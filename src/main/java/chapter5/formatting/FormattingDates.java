package chapter5.formatting;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class FormattingDates {
    public static void main(String[] args) {
        dates();
        System.out.println();
        times();
        System.out.println();
        dateTimes();
    }

    private static void dates() {
        System.out.println("Dates:");

        LocalDate date = LocalDate.now();
        System.out.println(DateTimeFormatter.ISO_LOCAL_DATE.format(date));
        // System.out.println(DateTimeFormatter.ISO_LOCAL_TIME.format(date)); // Throws UnsupportedTemporalTypeException!

        System.out.println("Parsed " + LocalDate.parse("2019-08-06", DateTimeFormatter.ISO_LOCAL_DATE));
    }

    private static void times() {
        System.out.println("Times:");

        LocalTime time = LocalTime.now();
        System.out.println(DateTimeFormatter.ISO_LOCAL_TIME.format(time));
        // System.out.println(DateTimeFormatter.ISO_LOCAL_DATE.format(time)); // Throws UnsupportedTemporalTypeException!

        System.out.println("Parsed " + LocalTime.parse("18:30", DateTimeFormatter.ISO_LOCAL_TIME));
    }

    private static void dateTimes() {
        System.out.println("Dates & times:");

        LocalDateTime dateTime = LocalDateTime.now();
        System.out.println("Local:");
        System.out.println(DateTimeFormatter.ISO_LOCAL_DATE.format(dateTime));
        System.out.println(DateTimeFormatter.ISO_LOCAL_TIME.format(dateTime));
        System.out.println(DateTimeFormatter.ISO_LOCAL_DATE_TIME.format(dateTime));
        // System.out.println(DateTimeFormatter.ISO_ZONED_DATE_TIME.format(dateTime)); // Throws UnsupportedTemporalTypeException!

        System.out.println("Parsed " + LocalDateTime.parse("2019-08-06T18:30:00.00", DateTimeFormatter.ISO_LOCAL_DATE_TIME));

        ZonedDateTime zonedDateTime = ZonedDateTime.now(ZoneId.of("Europe/Amsterdam"));
        System.out.println("Zoned:");
        System.out.println(DateTimeFormatter.ISO_LOCAL_DATE.format(zonedDateTime));
        System.out.println(DateTimeFormatter.ISO_LOCAL_TIME.format(zonedDateTime));
        System.out.println(DateTimeFormatter.ISO_LOCAL_DATE_TIME.format(zonedDateTime));
        System.out.println(DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(zonedDateTime));
        System.out.println(DateTimeFormatter.ISO_ZONED_DATE_TIME.format(zonedDateTime));

        System.out.println("Parsed " + ZonedDateTime.parse("2019-08-06T18:30:00.00+02:00[Europe/Amsterdam]", DateTimeFormatter.ISO_ZONED_DATE_TIME));
    }
}
