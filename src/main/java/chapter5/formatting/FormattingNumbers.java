package chapter5.formatting;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Currency;
import java.util.Locale;

public class FormattingNumbers {
    public static void main(String[] args) throws ParseException {
        numbers();
        System.out.println();
        currency();
        System.out.println();
        percentages();
    }

    private static void numbers() throws ParseException {
        System.out.println("Numbers:");
        NumberFormat nlFormat = NumberFormat.getInstance(new Locale("nl"));
        NumberFormat enFormat = NumberFormat.getInstance(Locale.ENGLISH);

        System.out.println("Formatting");
        System.out.println("  NL: " + nlFormat.format(123.45));
        System.out.println("  EN: " + enFormat.format(123.45));

        System.out.println("Parsing");
        System.out.println("  NL: " + nlFormat.parse("123,45"));
        System.out.println("  EN: " + enFormat.parse("123.45"));
    }

    private static void currency() throws ParseException {
        System.out.println("Currency:");
        Locale nl = new Locale("nl", "NL");
        NumberFormat nlFormat = NumberFormat.getCurrencyInstance(nl);
        nlFormat.setCurrency(Currency.getInstance(nl));
        NumberFormat enFormat = NumberFormat.getCurrencyInstance(Locale.UK);
        enFormat.setCurrency(Currency.getInstance(Locale.UK));

        System.out.println("Formatting");
        System.out.println("  NL: " + nlFormat.format(123.45));
        System.out.println("  EN: " + enFormat.format(123.45));

        System.out.println("Parsing");
        System.out.println("  NL: " + nlFormat.parse("€ 123,45"));
        System.out.println("  EN: " + enFormat.parse("£123.45"));
    }

    private static void percentages() throws ParseException {
        System.out.println("Percentages:");
        NumberFormat nlFormat = NumberFormat.getPercentInstance(new Locale("nl"));

        System.out.println("Formatting");
        System.out.println("  NL: " + nlFormat.format(0.123));

        System.out.println("Parsing");
        System.out.println("  NL: " + nlFormat.parse("12,34%"));
    }
}
